LIBS  =
CFLAGS = -std=c99 -Wall -Wextra

# All .c files
SRC=$(wildcard *.c)

IFJ: $(SRC)
	clear
	clear
	gcc -o $@ $^ $(CFLAGS) $(LIBS)
	
clean:
	rm -f IFJ
