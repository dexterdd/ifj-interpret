#include "semantic.h"


#define RC_DONE 11

const tRule Ptable [16][16] =
{
    //	          +  -  *  /  == != <= >= >  <  (  ) ID  F  ,  $
    /*0  +  */  { G, G, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*1  -  */  { G, G, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*2  *  */  { G, G, G, G, G, G, G, G, G, G, L, G, L, L, G, G },
    /*3  /  */  { G, G, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*4  == */  { L, L, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*5  != */  { L, L, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*6  <= */  { L, L, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*7  >= */  { L, L, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*8  >  */  { L, L, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*9 <  */   { L, L, L, L, G, G, G, G, G, G, L, G, L, L, G, G },
    /*10 (  */  { L, L, L, L, L, L, L, L, L, L, L, E, L, L, G, N },
    /*11 )  */  { G, G, G, G, G, G, G, G, G, G, N, G, N, N, G, G },
    /*12 ID */  { G, G, G, G, G, G, G, G, G, G, N, G, N, N, G, G },
    /*13 F  */  { N, N, N, N, N, N, N, N, N, N, L, G, N, N, N, N },
    /*14 ,  */  { L, L, L, L, L, L, L, L, L, L, L, E, L, L, N, N },
    /*15 $  */  { L, L, L, L, L, L, L, L, L, L, L, N, L, L, N, E }
};

// Vypise Error
//==============
int PT_Error(int error){
	static int status = RC_OK;

	// nulovani
	if(error == -1) status = RC_OK;
	// pokud status je RC_DONE, vrati RC_DONE
	if(status == RC_DONE) return status;
	// pokud se nezapisuje error, vraci aktualni stav
	if(error < RC_LexicalErr || error > RC_DONE){
		return status;
	}
	// pokud status je error, vrati error
	if(status > RC_OK && status < RC_DONE){
		return status;
	} 
	
	// jinak se zapise error
	if(PT_DEBUG) printf("PT_Error: %d\t", error);
	status = error;
	return status;
}

int expr(bool pure, char* dest, char** logicRes){
	tPTstack stack, nonTermStack;
	int returnCode;
	char* result;
	tItem temp, sBottom;
	bool Ass = false;

	PTSinit(&stack);
	PTSinit(&nonTermStack);

	// inicializace PT_Error
	PT_Error(-1);

	if(PT_DEBUG) printf("\n================== * Semantic * =================\n\n");

	get_token(token);
	if(pure == false && token->typ != E_Left_Bracket){
		return RC_SyntaxErr;
	}
	else if(token->typ == E_Assigment){
		Ass = true;
		get_token(token);
	}
	else if(token->typ == E_Left_Bracket){
		// Vlozi se dno zasobniku
		sBottom.idx = I_End;
		sBottom.symbol = NULL;
		PTSpush(&stack, &sBottom);
		// Vlozi se identifikator funkce do zasobniku
		temp.idx = I_Id;
		temp.symbol = dest;
		PTSpush(&stack, &temp);
	}
	else return RC_SyntaxErr;

	returnCode = utilizeToken(*token, &stack, &nonTermStack); 
	if(returnCode != RC_OK){
		if(returnCode == RC_DONE) return RC_SyntaxErr;
		return returnCode;
	}

	while(1){
		get_token(token);
		returnCode = utilizeToken(*token, &stack, &nonTermStack); 
		if(returnCode == RC_DONE) break;
		if(returnCode != RC_OK){
			return returnCode;
		}
	}

	if(PTStop(&nonTermStack) == NULL) return RC_SyntaxErr;
	result = (PTStopPop(&nonTermStack))->symbol;

	//Pokud se jedna o prirazeni, vysledek se ulozi do dest
	if(Ass){
		if(addInstruction(I_SET, dest, result, NULL) == NULL) return RC_InternalErr;
		if(PT_DEBUG) printf("=>\taddInstruction: I_SET, %s, %s\n", dest, result);
	}
	else if(!pure){
		*logicRes = result;
	}
	if(PT_DEBUG) printf("\n============= * Semantic Done * =============\n\n");
	return RC_OK;
}

// Zpracovani tokenu
//===================
int utilizeToken(tToken token, tPTstack* stack, tPTstack* nonTermStack){
	tItem* term;
	int rule;

	term = getIndex(&token);
	if(PT_Error(RC_OK) != RC_OK) return PT_Error(RC_OK);
	rule = getRule(term, stack, nonTermStack);
	if(PT_Error(RC_OK) != RC_OK) return PT_Error(RC_OK);
	if(rule == N) return RC_SyntaxErr;
	rule = useRule(rule, term, stack, nonTermStack);

	if(PT_DEBUG) printf("____________________________________________________________________\n\n");

	return rule;
}

// Vraci index do precedencni tabulky
//====================================
tItem* getIndex(tToken* token){
	if(PT_DEBUG) printf("getIndex\n");
	tItem* term;
	static int counter = 0;

	// vytvori se terminal
	term = malloc(sizeof(tItem));
	if(term == NULL){ PT_Error(RC_InternalErr); return NULL;}
	
	term->symbol = NULL;
	
	if(PT_DEBUG) printf("\tToken: %d\n", token->typ);

	switch(token->typ){
		case E_Plus:
			term->idx = I_Add;
			break;
		case E_Minus:
			term->idx = I_Sub;
			break;
		case E_Multiple:
			term->idx = I_Mul;
			break;
		case E_Divide:
			term->idx = I_Div;
			break;
		case E_Equal:
			term->idx = I_Equal;
			break;
		case E_Not_Equal:
			term->idx = I_NotEqual;
			break;
		case E_Lesser_Equal:
			term->idx = I_LessOrEq;
			break;
		case E_Greater_Equal:
			term->idx = I_GreatOrEq;
			break;
		case E_Greater:
			term->idx = I_Greater;
			break;
		case E_Lesser:
			term->idx = I_Lesser;
			break;
		case E_Left_Bracket:
			term->idx = I_LeftBr;
			counter++;
			break;
		case E_Right_Bracket:
			term->idx = I_RightBr;
			counter--;
			break;
		case E_Identif:
			term->symbol = token->value.s;
			term->idx = I_Id;
			break;
		case E_Int_Literal:
			term->symbol = generateId();
			term->idx = I_Id;
			printf("1\n");
			if(addInstruction(I_SETC, term->symbol, &token->value.i, (void*) T_INT) == NULL){PT_Error(RC_InternalErr); return NULL;}
			break;
		case E_Double_Literal:
			term->symbol = generateId();
			term->idx = I_Id;
			if(addInstruction(I_SETC, term->symbol, &token->value.d, (void*) T_DOUBLE) == NULL){PT_Error(RC_InternalErr); return NULL;}
			break;
		case E_String:
			term->symbol = generateId();
			term->idx = I_Id;
			if(addInstruction(I_SETC, term->symbol, &token->value.s, (void*) T_STRING) == NULL){PT_Error(RC_InternalErr); return NULL;}
			break;
		case E_True:
			term->symbol = generateId();
			term->idx = I_Id;
			if(addInstruction(I_SETC, term->symbol, &token->value.b, (void*) T_BOOL) == NULL){PT_Error(RC_InternalErr); return NULL;}
			break;
		case E_False:
			term->symbol = generateId();
			term->idx = I_Id;
			if(addInstruction(I_SETC, term->symbol, &token->value.b, (void*) T_BOOL) == NULL){PT_Error(RC_InternalErr); return NULL;}
			break;
		case E_Comma:
			
			term->idx = I_Comma;
			break;
		case E_Semicolon:
		case E_Left_Curly_Bracket:
			if(counter != 0){
				if(PT_DEBUG) printf("ERROR: Brackets\n"); 
				counter = 0; 
				PT_Error(RC_SyntaxErr); 
				return NULL;
			}
			term->idx = I_End;
			break; 
		default:
			PT_Error(RC_SyntaxErr);
			return NULL;
	}
	return term;
}

// Vraci pravidlo z precedencni tabulky
//======================================
tRule getRule(tItem* term, tPTstack* stack, tPTstack* nonTermStack){
	tItem* stackItem;
	tItem* sBottom;
	if(PT_DEBUG) printf("\ngetRule\n");

	// pokud je zasobnik prazdny, vlozi se dno zasobniku
	if(PTSempty(stack) == true){
		if(PT_DEBUG) printf("\tstack is empty -> insert $\n");
		sBottom = malloc(sizeof(tItem)); 
		if(sBottom == NULL){
			PT_Error(RC_InternalErr);
			return N;
		}
		sBottom->idx = I_End;
		sBottom->symbol = NULL;
		PTSpush(stack, sBottom);
	}

 	stackItem = PTStop(stack);
 	
 	// pokud za identifikatorem nasleduje zavorka
 	// nastavi se jako identifikator funkce
 	if(stackItem->idx == I_Id && term->idx == I_LeftBr){
 		stackItem->idx = I_Function;
		if(PT_DEBUG) printf("\tPush function to nonterm stack\n");
		PTSpush(nonTermStack, stackItem);
 	}

 	if(PT_DEBUG){ 
 		printf("\tstack index: %d\t", stackItem->idx);
 		printf("term index: %d\n", term->idx);
 		printf("\tgetRule: %d\t\n", Ptable[stackItem->idx][term->idx]);
 	}

 	return Ptable[stackItem->idx][term->idx];
}


// ===================================================
// |  * Zpracovani pravidla z precedencni tabulky *  |
// ===================================================
// potrebuje: vyhodnocene pravidlo, aktualni terminal, zasobnik, zasobnik na neterminaly
int useRule(tRule rule, tItem* term, tPTstack* stack, tPTstack* nonTermStack){
	tItem* stackItem;
	tItem* tempI;
	tItem* tempI2;
	tItem* nonTerm1;
	tItem* nonTerm2;
	char* symbol;
	int returnCode;


	if(PT_DEBUG) printf("\nuseRule\t");


	switch(rule){
		case L:		// <  	Terminal na zasobnik
			if(PT_DEBUG) printf(" L\n");
			
			PTSpush(stack, term);


			if(PT_DEBUG) printf("\tpush: tIndex %d\n", term->idx);
			if(PT_DEBUG) printf("*useRule L: done\n\n");
			break;


		case E:		// =  	Terminal na zasobnik
			if(PT_DEBUG) printf(" E\n");

			// pokud je na zasobniku $ -> konec
			if((PTStop(stack))->idx == I_End){
				if(PT_DEBUG) printf("*** The End ***\n");
				return RC_DONE;
				break;
			}
			PTSpop(stack);
			// pokud na zasobniku nasleduje funkce
			// -> funkce se odstrani
			tempI = PTStop(stack);	if(tempI == NULL) return RC_InternalErr;
			if(tempI->idx == I_Function){
				if(tempI->symbol == NULL){return RC_InternalErr;}

				// vytahne ze zasobniku neterminalu parametr funkce
				tempI2 = PTStop(nonTermStack);
				if(tempI2->idx != I_Function){
					// vlozi se parametr funkce
					if(PT_DEBUG) printf("=>\taddInstruction: I_SETpar, %s\n", tempI2->symbol);
					addInstruction(I_SETpar, tempI2->symbol, NULL, NULL);
					PTSpop(nonTermStack);
				}

				if(PT_DEBUG) printf("=>\taddInstruction: I_JMPfunc, %s\n", tempI->symbol);
				addInstruction(I_JMPfunc, tempI->symbol, NULL, NULL);

				PTSpop(stack);
				if(PT_DEBUG) printf("Function: *done\n");
			}
			 
			if(PT_DEBUG) printf("*useRule E: done\n\n");
			break;


		case G:		// >  	zpracuj prvni terminal ze zasobniku pred kterym je I_L "<"
			if(PT_DEBUG) printf(" G\n");
			stackItem = PTStopPop(stack);
			
			// ---- Na zasobniku je identifikator ----
			if(stackItem->idx == I_Id){
				if(PT_DEBUG) printf("\tCreate nonterminal \n");
				if(PT_DEBUG) printf("\tPush nonterm: %s\n", stackItem->symbol);
				PTSpush(nonTermStack, stackItem);	// vlozi se do zasobniku neterminalu
				
				if(PT_DEBUG) printf("*useRule G - Identifier: done\n\n");
				
				rule = getRule(term, stack, nonTermStack);				// znovu se vyhodnoti pravidlo
				
				returnCode = useRule(rule, term, stack, nonTermStack);	// znovu se pouzije pravidlo
				if(returnCode != RC_OK) return returnCode;
				break;
			}
			else

			// ---- Na zasobniku je "(" a na vstupu "," ----
			if(stackItem->idx == I_LeftBr && term->idx == I_Comma){
				if(PT_DEBUG) printf("\tUtitlize function parameter\n");

				// otestuje zda na zasobniku nasleduje identifikator funkce
				tempI = PTStop(stack);				
				if(tempI->idx != I_Function){return RC_SyntaxErr;}
				if(tempI->symbol == NULL){return RC_InternalErr;}

				// vytahne ze zasobniku neterminalu parametr funkce
				tempI = PTStopPop(nonTermStack);
				if(tempI->idx == I_Function){return RC_SyntaxErr;}
				// vlozi se parametr funkce
				if(PT_DEBUG) printf("=>\taddInstruction: I_SETpar, %s\n", tempI->symbol);
				addInstruction(I_SETpar, tempI->symbol, NULL, NULL);

				// leva zavorka se vlozi zpet do zasobniku
				PTSpush(stack, stackItem);
				break;
			}

			// ---- Na zasobniku je operator ----
			if( stackItem->idx == I_Add || stackItem->idx == I_Sub || stackItem->idx == I_Mul || stackItem->idx == I_Div ||
				stackItem->idx == I_Greater || stackItem->idx == I_Lesser || stackItem->idx == I_GreatOrEq || 
				stackItem->idx == I_LessOrEq || stackItem->idx == I_Equal || stackItem->idx == I_NotEqual)
			{	
				if(PT_DEBUG) printf("\tAdd instruction \n");

				// vytahne ze zasobniku posledni dva neterminaly
				nonTerm1 = PTStopPop(nonTermStack);	
				nonTerm2 = PTStopPop(nonTermStack);
				if(nonTerm1 == NULL || nonTerm2 == NULL) return RC_SyntaxErr;
				else if(nonTerm1->symbol == NULL || nonTerm2->symbol == NULL) return RC_InternalErr;

				symbol = malloc(sizeof(char)); if(symbol == NULL) {return RC_InternalErr;}
				// vytvoreni instrukce a ziskani ukazatele na vysledny symbol
				symbol = generOpInstruct(stackItem->idx, nonTerm2, nonTerm1);
				if(symbol == NULL){if(PT_DEBUG) printf("No result!\n"); return PT_Error(RC_OK);}

				// vlozeni vysledku do zasobniku neterminalu
				tempI = malloc(sizeof(tItem)); 	if(tempI == NULL) return RC_InternalErr;
				tempI->symbol = symbol;
				tempI->idx = I_Id;
				PTSpush(nonTermStack, tempI);
				if(PT_DEBUG) printf("*useRule G - Operator: done\n\n");

				// nove vyhodnoceni
				rule = getRule(term, stack, nonTermStack);
				returnCode = useRule(rule, term, stack, nonTermStack);
				if(returnCode != RC_OK) return returnCode;
				break;
			}
			else 

			if(stackItem->idx == I_LeftBr){
				if(PT_DEBUG) printf("*useRule G - Left bracket: done\n");
				return RC_InternalErr;
			}
			else

			if(stackItem->idx == I_RightBr){
				if(PT_DEBUG) printf("*useRule G - Right bracket: done\n");
				return RC_InternalErr;
			}
			else

			if(stackItem->idx == I_End){
				if(PT_DEBUG) printf("*useRule G - Stack empty ($)\n\n");
				return RC_InternalErr;
			}
			else
				if(PT_DEBUG) printf("*useRule G - No rule: done\n");
				if(PT_DEBUG) printf("\tstackItem: %d \t term: %d\n\n", stackItem->idx, term->idx);
				return RC_InternalErr;

		default:
			if(PT_DEBUG) printf("*useRule: error\n\n");
			return RC_SyntaxErr;
	}
	return RC_OK;
}




// Generuje instrukci matematicke nebo logicke operace
//=====================================================
char* generOpInstruct(tIndex oper, tItem* nonTerm1, tItem* nonTerm2){
	if(nonTerm1 == NULL || nonTerm2 == NULL){
		PT_Error(RC_InternalErr);
		return NULL;
	}

	char* sName;
	bool generTemp;


	if(*(nonTerm1->symbol) == '@'){
		sName = nonTerm1->symbol;
		generTemp = false;
	}
	else if(*(nonTerm2->symbol) == '@'){
		sName = nonTerm2->symbol;
		generTemp = false;
	}
	else {
		sName = generateId(); // generovani jmena identifikatoru docasne promenne
		generTemp = true;
	}


	switch(oper){
		case I_Add:
			if(PT_DEBUG) printf("=>\taddInstruction: I_ADD, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_INT) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}
			addInstruction(I_ADD, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_Sub:
			if(PT_DEBUG) printf("=>\taddInstruction: I_SUB, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_INT) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}
			addInstruction(I_SUB, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_Mul:
			if(PT_DEBUG) printf("=>\taddInstruction: I_MUL, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_INT) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}
			addInstruction(I_MUL, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_Div:
			if(PT_DEBUG) printf("=>\taddInstruction: I_DIV, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_INT) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}
			addInstruction(I_DIV, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_Greater:
			if(PT_DEBUG) printf("=>\taddInstruction: I_MORE, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_BOOL) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}			
			addInstruction(I_MORE, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_Lesser:
			if(PT_DEBUG) printf("=>\taddInstruction: I_LESS, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_BOOL) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}		
			addInstruction(I_LESS, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_GreatOrEq:
			if(PT_DEBUG) printf("=>\taddInstruction: I_MOREEQ, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_BOOL) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}			
			addInstruction(I_MOREEQ, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_LessOrEq:
			if(PT_DEBUG) printf("=>\taddInstruction: I_LESSEQ, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_BOOL) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}			
			addInstruction(I_LESSEQ, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_Equal:
			if(PT_DEBUG) printf("=>\taddInstruction: I_EQ, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_BOOL) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}			
			addInstruction(I_EQ, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		case I_NotEqual:
			if(PT_DEBUG) printf("=>\taddInstruction: I_NOTEQ, %s, %s, %s\n", sName, nonTerm1->symbol, nonTerm2->symbol);
			if(generTemp){
				if(addInstruction(I_SETC, sName, NULL,(void*) T_BOOL) == NULL){PT_Error(RC_InternalErr); return NULL;}
			}			
			addInstruction(I_NOTEQ, sName, nonTerm1->symbol, nonTerm2->symbol);
			break;
		default:
			PT_Error(RC_InternalErr);
			return NULL;
	}
	if(PT_DEBUG) printf("\tResult: %s\n", sName);
	return sName;
}

// Generuje unikatni identifikator
//=================================
char* generateId(){
	char* id;
	static int num = 0;

	id = malloc(sizeof(char)*6);
	if(id == NULL){
		PT_Error(RC_InternalErr);
		return NULL;
	}
	sprintf(id, "@%d", num);
	num = (num+1)%100000;

	return id;
}



// =================================================
// |   *   Zasobnik pro precedencni tabulku   *    |
// =================================================
// Inicializace &stacku
void PTSinit(tPTstack *stack){
    if(stack == NULL) PT_Error(RC_InternalErr);
    stack->top = NULL;
    return;
}

// Vlozime prvek na &stack
void PTSpush(tPTstack *stack, tItem* item){
    tElem* spare;
    if(stack == NULL || item == NULL) PT_Error(RC_InternalErr);

    // alokujeme element
    spare = malloc(sizeof(tElem));
    if (spare == NULL){
        PT_Error(RC_InternalErr);
        return;
    }

    // nastavime hodnoty
    spare->item = item;
    spare->lptr = stack->top;
    stack->top = spare;
}

// Odstranime prvek z topu stacku
void PTSpop(tPTstack* stack){
	tElem* spare;

	if(stack == NULL) PT_Error(RC_InternalErr);

    // kdyz neni prazdny
    if (stack->top != NULL){
        spare = stack->top;
        stack->top = stack->top->lptr;
        free(spare);
    }
}

// Precteme z topu &stacku
tItem* PTStop(tPTstack *stack){
	if(stack == NULL) PT_Error(RC_InternalErr);	

	if(PTSempty(stack)){
		if(PT_DEBUG) printf("Stack is empty!\n");
		return NULL;
	}
    return stack->top->item;
}

// Kombinace toppop
tItem* PTStopPop(tPTstack *stack){
	if(stack == NULL) PT_Error(RC_InternalErr);

    if (!(PTSempty(stack))){
    	tItem* spare;
        spare = PTStop(stack);
        PTSpop(stack);
        return spare;
    }
    if(PT_DEBUG) printf("Stack is empty!\n");
    return NULL;	
}

// Kontrola zda neni prazdny
bool PTSempty(tPTstack *stack){
    if(stack == NULL){ 
    	PT_Error(RC_InternalErr);
    	return true;
    }
	
	if(stack->top != NULL)
        return false;

    return true;
}
// ======== Konec zasobniku ========


