#ifndef ERROR_H
#define ERROR_H

enum returnCodes{
	RC_OK = 0,
	
// chyba v programu v rámci lexikální analýzy (chybná struktura aktuálního lexému).
	RC_LexicalErr = 1,
	
// chyba v programu v rámci syntaktické analýzy (chybná syntaxe programu).
	RC_SyntaxErr = 2,
	
// sémantická chyba v programu – nedefinovaná tˇrída/funkce/promenná, pokus o redefinici trídy/funkce/promenné, atd.
	RC_SemDefinitionErr = 3,
	
// sémantická chyba typové kompatibility v aritmetických, retezcových a relacních výrazech, príp. špatný pocet ci typ parametru u volání funkce.
	RC_SemTypeErr = 4,
	
// ostatní sémantické chyby
	RC_SemOtherErr = 6,
	
// behová chyba pri nacítání císelné hodnoty ze vstupu.
	RC_RunInputErr = 7,
	
// behová chyba pri práci s neinicializovanou promennou.
	RC_RunVariableErr = 8,
	
// behová chyba delení nulou.
	RC_RunDivisonByZeroErr = 9,
	
// ostatní behové chyby.
	RC_RunOtherErr = 10,
	
// interní chyba interpretu tj. neovlivnená vstupním programem (napr. chyba alokace pameti, chyba pri otvírání souboru s rídicím programem, špatné parametry príkazové rádky atd.)
	RC_InternalErr = 99
};

#endif 
