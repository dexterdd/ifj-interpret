#include "interpret.h"


/* INTERPRETER */
int runInterpreter()
{
	tSymbol *out,*in1,*in2;
	bool b_in1, b_in2;
	double d_in1, d_in2;
	int i_in1, i_in2;
	char *s_in1, *s_in2;
	tSymbol symbol;
	tInstruction *i;
	int returnCode = RC_OK;
	
	while(true){
		nextInstruction();
		if(DEBUG){
			printf("\nscope: %s ------------------------------------\n\n",generateSymbolName(""));
			printInstructions();
			printSymbolTable();
		}
		i = getActiveInstruction();
		if(i == NULL) return RC_InternalErr; 	// seznam nekonci I_HALT;
		
		/* parametr out */
		switch(i->type){
			case I_NOP:
			case I_JMP:
			case I_JMPT:
			case I_JMPF:
			case I_HALT:
				break;

			case I_SETC:
			case I_SET:
				out = getSymbol( i->out );
				if(out == NULL){
					symbol.key = generateSymbolName((char*)i->out);
					symbol.isFunction = false;
					symbol.nextArg = NULL;
					symbol.begin = NULL;
					
					if(i->type == I_SETC) {
						symbol.isDefined = i->in1!=NULL;
						symbol.type = (symbolType) i->in2;
					} else if(i->type == I_SET){
						symbol.isDefined = true;
						symbol.type = T_UNDEF;
					}
					out = insertSymbol(symbol);
					if(out == NULL) return RC_InternalErr;
				}
				break;

			case I_WRITE:
				out = getSymbol( i->out );
				if(out == NULL) return RC_RunVariableErr;
				if(!out->isDefined) return RC_SemDefinitionErr;
			
			default:
				out = getSymbol( i->out );
				if(out == NULL) return RC_RunVariableErr;
		}

		/* parametr in1 */
		switch(i->type){
			case I_SET:
			case I_ADD:
			case I_SUB:
			case I_MUL:
			case I_DIV:
			case I_NOT:
			case I_AND:
			case I_OR:
				in1 = getSymbol( i->in1 );
				
				if(in1 == NULL) return RC_RunVariableErr;
				if(!in1->isDefined) return RC_SemDefinitionErr;
				
				if(i->type == I_SET && out->type==T_UNDEF) out->type = in1->type; 
				
				if (out->type == in1->type){
					b_in1 = in1->value.b;
					d_in1 = in1->value.d;
					i_in1 = in1->value.i;
					s_in1 = (char*) in1->value.s;
				}
				else if (out->type == T_BOOL && in1->type == T_DOUBLE)
					b_in1 = in1->value.d == 0.0;
				else if (out->type == T_BOOL && in1->type == T_INT)
					b_in1 = in1->value.i == 0;
				else if (out->type == T_DOUBLE && in1->type == T_INT)
					d_in1 = (double) in1->value.i;
				else if (out->type == T_INT && in1->type == T_DOUBLE)
					i_in1 = (int) in1->value.d;
				else
					return RC_SemTypeErr;
				break;

			case I_EQ:
			case I_NOTEQ:
			case I_MORE:
			case I_LESSEQ:
			case I_MOREEQ:
				in1 = getSymbol( i->in1 );
				if(in1 == NULL) return RC_RunVariableErr;
				if(!in1->isDefined) return RC_SemDefinitionErr;

				b_in1 = in1->value.b;
				d_in1 = in1->value.d;
				i_in1 = in1->value.i;
				s_in1 = (char*) in1->value.s;

			default:
				break;
		}

		/* parametr in2 */
		switch(i->type){
			case I_SETC:
				symbol.type = (symbolType) i->in2;
				break;
							
			case I_ADD:
			case I_SUB:
			case I_MUL:
			case I_DIV:
			case I_AND:
			case I_OR:
				in2 = getSymbol( i->in2 );
				if(in2 == NULL) return RC_RunVariableErr;
				if(!in2->isDefined) return RC_SemDefinitionErr;

				if (out->type == in2->type){
					b_in2 = in2->value.b;
					d_in2 = in2->value.d;
					i_in2 = in2->value.i;
					s_in2 = (char*) in2->value.s;
				}
				else if (out->type == T_BOOL && in2->type == T_DOUBLE)
					b_in2 = in2->value.d == 0.0;
				else if (out->type == T_BOOL && in2->type == T_INT)
					b_in2 = in2->value.i == 0;
				else if (out->type == T_DOUBLE && in2->type == T_INT)
					d_in2 = (double) in2->value.i;
				else if (out->type == T_INT && in2->type == T_DOUBLE)
					i_in2 = (int) in2->value.d;
				else
					return RC_SemTypeErr;
				break;

			case I_EQ:
			case I_NOTEQ:
			case I_MORE:
			case I_LESSEQ:
			case I_MOREEQ:
				in2 = getSymbol( i->in2 );
				if(in2 == NULL) return RC_RunVariableErr;
				if(!in2->isDefined) return RC_SemDefinitionErr;

				if (in1->type == in2->type){
					b_in2 = in2->value.b;
					d_in2 = in2->value.d;
					i_in2 = in2->value.i;
					s_in2 = (char*) in2->value.s;
				}
				else if (in1->type == T_BOOL && in2->type == T_DOUBLE)
					b_in2 = in2->value.d == 0.0;
				else if (in1->type == T_BOOL && in2->type == T_INT)
					b_in2 = in2->value.i == 0;
				else if (in1->type == T_DOUBLE && in2->type == T_INT)
					d_in2 = (double) in2->value.i;
				else if (in1->type == T_INT && in2->type == T_DOUBLE)
					i_in2 = (int) in2->value.d;
				else
					return RC_SemTypeErr;

			default:
				break;
		}
		
		
		
		/* provedeni instrukce */
		switch(i->type){
			case I_NOP:
				break;
		
			case I_SETC:
				if(out->isDefined)
					switch(out->type){
						case T_BOOL:
							out->value.b = *((bool*) i->in1);
							break;
						case T_DOUBLE:
							out->value.d = *((double*) i->in1);
							break;
						case T_INT:
							out->value.i = *((int*) i->in1);
							break;
						case T_STRING:
							out->value.s = ((char*) i->in1);
							break;
						default:
							return RC_RunOtherErr;
					}
				break;
				
			case I_SET:
				switch(out->type){
					case T_BOOL:
						out->value.b = b_in1;
						break;
					case T_DOUBLE:
						out->value.d = d_in1;
						break;
					case T_INT:
						out->value.i = i_in1;
						break;
					case T_STRING:
						out->value.s = s_in1;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->isDefined = true;
				break;
				
			case I_ADD:
				switch(out->type){
					case T_DOUBLE:
						out->value.d = d_in1 + d_in2;
						break;
					case T_INT:
						out->value.i = i_in1 + i_in2;
						break;
					/*case T_STRING:
						out->value.s = appendString();
						break;*/
					default:
						return RC_SemTypeErr;
				}
				out->isDefined = true;
				break;
				
			case I_SUB:	
				switch(out->type){
					case T_DOUBLE:
						out->value.d = d_in1 - d_in2;
						break;
					case T_INT:
						out->value.i = i_in1 - i_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->isDefined = true;
				break;
				
			case I_MUL:		
				switch(out->type){
					case T_DOUBLE:
						out->value.d = d_in1 * d_in2;
						break;
					case T_INT:
						out->value.i = i_in1 * i_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->isDefined = true;
				break;
				
			case I_DIV:	
				switch(out->type){
					case T_DOUBLE:
						if(d_in1 == 0.0) return RC_RunDivisonByZeroErr;
						out->value.d = d_in1 / d_in2;
						break;
					case T_INT:
						if(i_in1 == 0) return RC_RunDivisonByZeroErr;
						out->value.i = i_in1 / i_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->isDefined = true;
				break;
				
			case I_NOT:
				switch(out->type){
					case T_BOOL:
						out->value.b = !b_in1;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;	
				
			case I_AND:
				switch(out->type){
					case T_BOOL:
						out->value.b = b_in1 && b_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;
				
			case I_OR:
				switch(out->type){
					case T_BOOL:
						out->value.b = b_in1 || b_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;
				
			case I_EQ:
				switch(in1->type){
					case T_BOOL:
						out->value.b = b_in1 == b_in2;
						break;
					case T_DOUBLE:
						out->value.b = d_in1 == d_in2;
						break;
					case T_INT:
						out->value.b = i_in1 == i_in2;
						break;
					case T_STRING:
						out->value.b = !strcmp(s_in1, s_in2);
						break;
					default:
						return RC_RunOtherErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;
				
			case I_NOTEQ:
				switch(in1->type){
					case T_BOOL:
						out->value.b = b_in1 != b_in2;
						break;
					case T_DOUBLE:
						out->value.b = d_in1 != d_in2;
						break;
					case T_INT:
						out->value.b = i_in1 != i_in2;
						break;
					case T_STRING:
						out->value.b = strcmp(s_in1, s_in2);
						break;
					default:
						return RC_RunOtherErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;
				
			case I_LESS:
				switch(in1->type){
					case T_DOUBLE:
						out->value.b = d_in1 < d_in2;
						break;
					case T_INT:
						out->value.b = i_in1 < i_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;
				
			case I_MORE:
				switch(in1->type){
					case T_DOUBLE:
						out->value.b = d_in1 > d_in2;
						break;
					case T_INT:
						out->value.b = i_in1 > i_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;
				
			case I_LESSEQ:
				switch(in1->type){
					case T_DOUBLE:
						out->value.b = d_in1 <= d_in2;
						break;
					case T_INT:
						out->value.b = i_in1 <= i_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;
				
			case I_MOREEQ:
				switch(in1->type){
					case T_DOUBLE:
						out->value.b = d_in1 >= d_in2;
						break;
					case T_INT:
						out->value.b = i_in1 >= i_in2;
						break;
					default:
						return RC_SemTypeErr;
				}
				out->type = T_BOOL;
				out->isDefined = true;
				break;
				
			case I_INC:
				switch(out->type){
					case T_INT:
						out->value.i++;
						break;
					default:
						return RC_SemTypeErr;
				}
				break;
				
			case I_DEC:
				switch(out->type){
					case T_INT:
						out->value.i--;
						break;
					default:
						return RC_SemTypeErr;
				}
				break;	
				
			case I_READ:
				switch(out->type){
					case T_BOOL:
						returnCode = readBool( &(out->value.b) );
						if(returnCode != RC_OK) return returnCode;
						break;
					case T_DOUBLE:
						returnCode = readDouble( &(out->value.d) );
						if(returnCode != RC_OK) return returnCode;
						break;
					case T_INT:
						returnCode = readInt( &(out->value.i) );
						if(returnCode != RC_OK) return returnCode;
						break;
					case T_STRING:
						readString( out->value.s );
						break;
					default:
						return RC_RunOtherErr;
				}
				out->isDefined = true;
				break;
			
			case I_WRITE:
				switch(out->type){
					case T_BOOL:
						printBool( out->value.b );
						break;
					case T_DOUBLE:
						printDouble( out->value.d );
						break;
					case T_INT:
						printInt( out->value.i );
						break;
					case T_STRING:
						print( out->value.s );
						break;
					default:
						return RC_RunOtherErr;
				}
				printf("\n");
				break;
				
			case I_JMP:
				if(out == NULL) return RC_InternalErr;
				
				returnCode = goToInstruction( (tInstruction*) i->out);
				if(returnCode != RC_OK) return RC_InternalErr;
				break;
				
			case I_JMPT:
				in1 = getSymbol( i->in1 );
				if(out == NULL
				|| in1 == NULL) return RC_InternalErr;
				
				if(in1->value.b){
					returnCode = goToInstruction( (tInstruction*) i->out);
					if(returnCode != RC_OK) return RC_InternalErr;
				}
				break;
				
			case I_JMPF:
				in1 = getSymbol( i->in1 );
				if(out == NULL
				|| in1 == NULL) return RC_InternalErr;
				
				if(!in1->value.b){
					returnCode = goToInstruction( (tInstruction*) i->out);
					if(returnCode != RC_OK) return RC_InternalErr;
				}
				break;
				
			case I_JMPfunc:
				if(!out->isFunction) return RC_SemDefinitionErr;	//id neni funkce
				
				returnCode = addJump();		// ulozeni infomace o soucasne instrukci a scopu
				if(returnCode != RC_OK) return RC_InternalErr;
				
				tSymbol *needed = out->nextArg;
				tParamItem *given = getFirstParam();
				
				while(needed != NULL){
					if(given == NULL) return RC_SemTypeErr;	// malo parametru
					
					needed = needed->nextArg;
					given = given->next;
				}	if(given != NULL) return RC_SemTypeErr;	// moc parametru
				needed = out->nextArg;
				given = getFirstParam();
				
				
				// vestavene (nativni) funkce
				if(!strcmp(i->out,"ifj16.length")) {
					out->value.i = length(	given->s->value.s);
					break;
				} if(!strcmp(i->out,"ifj16.substr")) {
					out->value.s = substr(	given->s->value.s,
											given->next->s->value.i,
											given->next->next->s->value.i);
					if(out->value.s == NULL) return RC_RunOtherErr;
					break;
				} if(!strcmp(i->out,"ifj16.compare")) {
					out->value.i = compare(	given->s->value.s,
											given->next->s->value.s);
					break;
				} if(!strcmp(i->out,"ifj16.find")) {
					out->value.i = find(	given->s->value.s,
											given->next->s->value.s);
					break;
				} if(!strcmp(i->out,"ifj16.sort")) {
					out->value.s = sort(	given->s->value.s);
					if(out->value.s == NULL) return RC_InternalErr;
					break;
				}
				
				// prechod do scopu funkce
				returnCode = setGlobalScopeList(&out->funcScope);
				if(returnCode != RC_OK) return RC_InternalErr;
				
				// zjisteni jmena funkce Class.name.name.name -> "name"
				char *c = out->key;
				int l1 = 0, l2 = 0;
				while(*c!='\0'){
					l1 = *c=='.' ? 0 : l1+1;
					l2++;
					c++;
				}
				c = malloc(l1);
				if(c==NULL) return RC_InternalErr;
				for(int i=0; i<=l1; i++) c[i] = out->key[l2-l1+i];
							
				for(int i=0; i <= out->running; i++){
					returnCode = addScopeLabel(c);
					if(returnCode!=RC_OK) return RC_InternalErr;
				}
				
				while(needed != NULL){
					symbol.key = generateSymbolName(needed->key);
					symbol.type = needed->type;
					symbol.isFunction = false;
					symbol.isDefined = true;
					symbol.nextArg = NULL;
					symbol.begin = NULL;
					
					if (needed->type == given->s->type)
						switch(needed->type){
							case T_BOOL: 	symbol.value.b = given->s->value.b; break;
							case T_DOUBLE: 	symbol.value.d = given->s->value.d; break;
							case T_INT: 	symbol.value.i = given->s->value.i; break;
							case T_STRING: 	symbol.value.s = given->s->value.s; break;
							default: return RC_InternalErr;
						}
					else if (needed->type == T_BOOL && given->s->type == T_DOUBLE)
						symbol.value.b = given->s->value.d == 0.0;
					else if (needed->type == T_BOOL && given->s->type == T_INT)
						symbol.value.b = given->s->value.i == 0;
					else if (needed->type == T_DOUBLE && given->s->type == T_INT)
						symbol.value.d = (double) given->s->value.i;
					else if (needed->type == T_INT && given->s->type == T_DOUBLE)
						symbol.value.i = (int) given->s->value.d;
					else
						return RC_SemTypeErr;
					
					if(insertSymbol(symbol) == NULL) return RC_InternalErr;
					
					needed = needed->nextArg;
					given = given->next;
				}
				if( goToInstruction( out->begin ) != RC_OK ) return RC_InternalErr;
				
				clearParamList();
				
				out->running++;
				break;
				
			case I_JMPback:
				if(!out->isFunction) return RC_SemDefinitionErr;	//id neni funkce
				
				returnCode = getJump();
				if(returnCode != RC_OK) return RC_InternalErr;
				
				removeLastJump();
				out->running--;
				break;
				
			case I_SETpar:
				if( insertParam(out) == NULL ) return RC_InternalErr;
				break;
				
			case I_HALT:
				return RC_OK;
				
			default:
				return RC_InternalErr;
		}
	}
}
/* INTERPRETER END */



