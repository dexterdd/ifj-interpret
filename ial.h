#ifndef IAL_H
#define IAL_H

#define DEBUG false

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "error.h"



/* INSTRUCTION LIST */
typedef enum {
	I_NOP,		// prazdna instrukce
	I_SETC,		// out = const type
	I_SET,		// out = in1
	I_ADD,		// out = in1 + in2
	I_SUB,		// out = in1 - in2
	I_MUL,		// out = in1 * in2
	I_DIV,		// out = in1 / in2
	I_NOT,		// out = !in1
	I_AND,		// out = in1 && in2
	I_OR,		// out = in1 || in2
	I_EQ,		// out = in1 == in2
	I_NOTEQ,	// out = in1 != in2
	I_LESS,		// out = in1 < in2
	I_MORE,		// out = in1 > in2
	I_LESSEQ,	// out = in1 <= in2
	I_MOREEQ,	// out = in1 >= in2
	I_INC,		// out++
	I_DEC,		// out--
	I_READ,		// to (podle typu symbolu out)
	I_WRITE,	// from (podle typu symbolu out)
	I_JMP,		// where
	I_JMPT,		// where in1=true
	I_JMPF,		// where in1=false
	I_JMPfunc,	// funcName
	I_JMPback,	// funcName
	I_SETpar,	// out
	I_HALT		// end of program
} instructionType;

typedef struct sInstruction tInstruction;
struct sInstruction {
	instructionType type;
	void* out;
	void* in1;
	void* in2;
	tInstruction* next;
};

typedef struct sInstructionList {
	tInstruction* first;
	tInstruction* last;
	tInstruction* active;
} *tInstructionListPtr;

tInstructionListPtr list;	// globalni seznam instrukci

int initInstructionList();
void freeInstructionList();
tInstruction *createInstruction(instructionType type, void* out, void* in1, void* in2);
tInstruction *addInstruction(instructionType type, void* out, void* in1, void* in2);
tInstruction *addExistingInstruction(tInstruction *i);
void nextInstruction();		// aktivuje dalsi, pripadne prvni instrukci
tInstruction *getActiveInstruction();
tInstruction *getLastInstruction();
int goToInstruction(tInstruction *to);
void printInstructions();				// debug
int getIDbyPtr(tInstruction *wanted);	// debug
/* INSTRUCTION LIST END */



/* SCOPE */
typedef struct sScopeItem tScopeItem;
struct sScopeItem {
	char *label;
	tScopeItem *prev;
	tScopeItem *next;
};

typedef struct sScopeList {
	tScopeItem *first;
	tScopeItem *last;
} *tScopeListPtr;

// GLOBAL
tScopeListPtr gsl;	// globalni seznam scope labelu

int initScopeList();
int addScopeLabel(char* l);
char* generateSymbolName(char* n);
void removeLastScopeLabel();
void clearScope();
void freeScopeList();

// LOCAL
int initLocalScopeList(tScopeListPtr *lsl);
int copyGlobalScopeList(tScopeListPtr *lsl);
int setGlobalScopeList(tScopeListPtr *lsl);
void clearLocalScope(tScopeListPtr *lsl);
void freeLocalScopeList(tScopeListPtr *lsl);

// SPECIAL
void freeAllScopes();	// nutne projit vsechny symboly
/* SCOPE END */



/* JUMP STACK */
typedef struct sJumpItem tJumpItem;
struct sJumpItem {
	tInstruction *from;
	tScopeListPtr currScope;
	tJumpItem *prev;
	tJumpItem *next;
};

typedef struct sJumpList *tJumpListPtr;
struct sJumpList {
	tJumpItem *first;
	tJumpItem *last;
};

tJumpListPtr jl;	// globalni zasobnik s informacemi o volani funkci

int initJumpList();
int addJump();
int getJump();
void removeLastJump();
void freeJumpList();
/* JUMP STACK END */



/* SYMBOL TABLE */
typedef union Value {
	bool b;
	double d;
	int i;
	char *s;
} tValue;

typedef enum {
	T_VOID,
	T_BOOL,
	T_DOUBLE,
	T_INT,
	T_STRING,
	T_UNDEF
} symbolType;

typedef struct sSymbol tSymbol;
struct sSymbol {
	char *key;
	symbolType type;
	bool isFunction;
	bool isDefined;
	tValue value;				// hodnota prommenne / navratova hodnota
	tSymbol *nextArg;			// dalsi argument funkce
	tScopeListPtr funcScope;	// prislusny scope
	tInstruction *begin;		// pocatecni instrukce
	int running;				// pocet bezicich instanci
};
 
typedef struct sSymbolItem *tSymbolItemPtr;	// uzel tabulky symbolu
struct sSymbolItem {
	tSymbol symbol;
	tSymbolItemPtr left;  
	tSymbolItemPtr right; 
};

tSymbolItemPtr root;	// globalni koren tabulky symbolu

void initSymbolTable();
tSymbolItemPtr insertIntoSymbolTable(tSymbolItemPtr *root, tSymbol symbol);
tSymbol *searchInSymbolTable(tSymbolItemPtr *root, char *key);
void printSymbolTable();
void freeSubtree(tSymbolItemPtr *root);
void freeSymbolTable();

tSymbol *insertBool(char* key, bool b);
tSymbol *insertInt(char* key, int i);
tSymbol *insertDouble(char* key, double d);
tSymbol *insertString(char* key, char* s);
tSymbol *insertSymbol(tSymbol symbol);
tSymbol *insertFunction(char* key, symbolType type);
tSymbol *addArgument(char* funcKey, char* argKey, symbolType argType);
tSymbol *getFunction(char* key);
tSymbol *getSymbol(char* key);
/* SYMBOL TABLE END */



/* PARAM LIST */
typedef struct sParamItem tParamItem;
struct sParamItem {
	tSymbol *s;
	tParamItem *next;
};

typedef struct sParamList *tParamListPtr;
struct sParamList {
	tParamItem *first;
	tParamItem *last;
};

tParamListPtr gpl;	// globalni seznam paramatru pro volani funkce

int initParamList();
tSymbol *insertParam(tSymbol *s);
tParamItem *getFirstParam();
void clearParamList();
void freeParamList();
/* PARAM LIST END */



char *shellSort(char *s);

int BMFind(char *x, char *str);

int max (int a, int b);

#endif
