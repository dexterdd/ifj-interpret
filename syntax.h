/*
IFJ-Interpret jazyka IFJ16
syntax.h
Syntax analýza + semanticke akce

Varianta: b/3/I
Tým 093
   Mikulík Petr.....xmikul57
   Matuš Adam.......xmatus31
   Měrka Filip......xmerka04
   Svoboda David....xsvobo0t
*/

#pragma once

#define M_DEFS 0
#define M_ALL 1

typedef struct sClassDef tClassDef;
struct sClassDef {
	char *classId;
	tClassDef *next;
};

typedef struct sClassList {
	tClassDef *first;
	tClassDef *last;
} tClassList;

extern tToken *token;

symbolType currentType;
symbolType currentFuncType;
char *currentIdentif;
char *currentFuncId;
int parseMode;
int tokenReady;

char *classId[20];
int classIdTop;

int insertClassId(char *cid);

// funkce pro syntaktickou analyzu
int expr(symbolType expectedType);
int paramList();
int param();
int assign();
int statementList();
int varDef();
int funcBody();
int paramDefList();
int paramDef();
int definition();
int classBody();
int classList();
int program();
int parse();
