#include "ifj16.h"

char line[1024];

int readBool(bool* b)
{
	scanf("%[^\n]", line);
	
	if(!strcmp(line,"true")) *b = true;
	else if(!strcmp(line,"false")) *b = false;
	else return RC_RunInputErr;
	return RC_OK;
}
int readDouble(double* d)
{
	char *ptr;
	scanf("%[^\n]", line);
	
	*d = strtod(line, &ptr);
	if(*ptr != '\0') return RC_RunInputErr;
	return RC_OK;
}
int readInt(int* i)
{
	char *ptr;
	scanf("%[^\n]", line);
	
	*i = strtol(line, &ptr, 0);
	if(*ptr != '\0') return RC_RunInputErr;
	return RC_OK;
}
void readString(char* s)
{
	scanf("%[^\n]", s);
}



void printBool(bool b)
{
	printf("%s", (b)?("true"):("false"));
}
void printDouble(double d)
{
	printf("%lf",d);
}
void printInt(int i)
{
	printf("%i",i);
}
void print(char* s)
{
	printf("%s",s);
}


int length(char* s)
{
	if(s==NULL) return 0;
	return strlen(s);
}
char* substr(char* s, int i, int n)
{
	if(strlen(s) < (size_t) i+n) return NULL;
	char* sub = malloc(n+1);
	if(sub != NULL)
		memcpy(sub, s+i, n);
	return sub;
}

int compare(char* s1, char* s2)
{
	int r = strcmp(s1, s2);
	if(r < 0) return 1;
	if(r > 0) return -1;
	return 0;
}

int find(char* s, char* search)
{
	return BMFind(search, s);
}

char* sort(char* s)
{
	if(s==NULL) return s;
	
	char* str = malloc(strlen(s)+1);
	if(str == NULL) return NULL;
	strcpy(str, s);	

	return shellSort(s);
}



