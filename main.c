#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "error.h"
#include "ial.h"
#include "lexical.h"
#include "syntax.h"
#include "sematic.h"
#include "interpret.h"


int main(int argc, char **argv)
{
	FILE *f;
	if (argc == 1){
		fprintf(stderr, "Neni zadan vstupni soubor.\n");
		return RC_InternalErr;
	}
	if ((f = fopen(argv[1], "r")) == NULL){
		fprintf(stderr, "Soubor se nepodarilo otevrit.\n");
		return RC_InternalErr;
	}
	setSourceFile(f);
	
	int returnCode = RC_OK;
	do{
		initSymbolTable();		// vytvoreni tabulky symbolu
		returnCode = initInstructionList();	// vytvoreni seznamu instrukci
		if(returnCode != RC_OK) break;
	
		returnCode = initJumpList();
		if(returnCode != RC_OK) break;
		
		returnCode = initScopeList();
		if(returnCode != RC_OK) break;
		
		returnCode = initParamList();
		if(returnCode != RC_OK) break;
		
		returnCode = parseSource();
		if(returnCode != RC_OK) break;
		
		returnCode = runInterpreter();
			
	} while(false);	// kvuli pouziti break;
	
	printf("\nreturnCode = %d\n", returnCode);	// nakonec ODSTRANIT
	
	freeParamList();
	freeScopeList();
	freeJumpList();
	freeInstructionList();
	freeSymbolTable();	// dealokace pameti
	fclose(f);
	return returnCode;
}



