/*
IFJ-Interpret jazyka IFJ16
lexical.c
Lexikální analýza

Varianta: b/3/I
Tým 093
   Mikulík Petr.....xmikul57
   Matu Adam.......xmatus31
   Mìrka Filip......xmerka04
   Svoboda David....xsvobo0t
*/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lexical.h"
#include <math.h>  

//vytvoreni tokenu
tToken *create_token()
{
	tToken *tok = malloc(sizeof(struct SToken));
	tok->typ = -1;
	return tok;
}

//odstraneni tokenu
void destroy_token(tToken *tok)
{
	if(tok == NULL) 
		return;
	if(tok->value.s != NULL)	
		free(tok->value.s);
	free(tok);
}
//hlavni cast
int get_token(tToken * token)
{	
	
	token->value.s = NULL;	
	int i = 0;
	int integerPomoc = 0;
	double doublePomoc = 0;
	static char znak = '&';
	if (znak == '&')
	{
		znak = fgetc(s_file);
	}
	char stav = 's';
	int returnCode = RC_OK;
	char* pomoc = NULL;
	while (znak != EOF)
	{
		switch (stav)	//prepnuti aktualniho stavu
		{
			case 's':		//pocatecni stav
			{
					if ((znak == '\t') || (znak == '\n') || (znak == ' '))  //bily znak, neprovede se nic
					{	
						znak = fgetc(s_file);
						stav = 's';
						break;
					}
					else if ( isalpha(znak) || (znak == '_') || (znak == '$'))  //identifikator
					{
						stav = 'j';
						break;
					}
					else if (isdigit(znak))							//ciselny literal
					{
						stav = 'c';
						break;
					}
					else if (znak == '+')							//plus
					{
						stav = 'p';
						break;
					}
					else if (znak == '-') 							// minus
					{
						stav = 'm';
						break;
					}
					else if (znak == '*') 							// nasobeni
					{
						stav = 'n';
						break;
					}
					else if (znak == '/') 							// deleni/komentar
					{
						stav = 'd';
						break;
					}
					else if (znak == '<') 							// mensi
					{
						stav = 'l';
						break;
					}
					else if (znak == '>') 							// vetsi
					{
						stav = 'g';
						break;
					}
					else if (znak == '=') 							// prirazeni
					{
						stav = 'a';
						break;
					}
					else if (znak == ';') 							// strednik
					{
						stav = 'q';
						break;
					}
					else if (znak == ',') 							// carka
					{
						stav = 'w';
						break;
					}
					else if (znak == '!') 							// vykricnik
					{
						stav = 'v';
						break;
					}
					else if (znak == '(') 							// leva zavorka
					{
						stav = 'b';
						break;
					}
					else if (znak == ')') 							// prava zavorka
					{
						stav = 'x';
						break;
					}
					else if (znak == '{') 							// leva slozena zavorka
					{
						stav = 'h';
						break;
					}
					else if (znak == '}') 							// prava slozena zavorka
					{
						stav = 'k';
						break;
					}
					else if (znak == '"') 							// dvojty uvozovky, retezcovy literal
					{
						stav = 'r';
						break;
					}
					else
					{
						returnCode = RC_LexicalErr;
						return returnCode;
						break;
					}
			}
			case 'j':		//identifikator
			{
				int count = 1;			
				token->typ = E_Identif;
				while (isalnum(znak) || (znak == '_') || (znak == '$')) //dokud je to cislice, pismeno, podtrzitko, dolar
				{
					znak = fgetc(s_file);
					count++;
				}
				if (znak == '.')	//pokud nasleduje tecka bude to plne kvalifikovany identifikator
				{
					token->typ = E_PKIdentif;
					znak = fgetc(s_file);
					count++;
					if (isalnum(znak) || (znak == '_') || (znak == '$'))	//za teckou musi nasledovat cislice, pismeno, podtrzitko, dolar, jinak je to lexikalni chyba
					{				
						while (isalnum(znak) || (znak == '_') || (znak == '$')) //dokud je to cislice, pismeno, podtrzitko, dolar
						{	
							znak = fgetc(s_file);
							count++;
						}
					}
					else			//lexikalni chyba
					{
						returnCode = RC_LexicalErr;
						return returnCode;
						break;	
					}
				}
				
				pomoc = malloc(count * sizeof(char));		//alokace promenne pomoc podle hodnoty v count
				//pokud se alokace nezdarila
				if (pomoc == NULL)			
				{
					returnCode = RC_InternalErr;
					break;
				}
					
				fseek(s_file, -count, SEEK_CUR);		//vraceni v souboru na pozici zacatku indentifikatoru
				for (i = 0; i < count-1; i++)			//nacteni do pomoc identifikator
					pomoc[i] = fgetc(s_file);
				pomoc[i] = '\0';
				//pokud identifaktor je nejake klicove slovo
				if ((strcmp(pomoc,"boolean") == 0))
				{
					token->typ = E_Boolean;
				}
				else if (strcmp(pomoc,"break") == 0)
				{
					token->typ = E_Break;
				}
				else if (strcmp(pomoc,"class") == 0)
				{
					token->typ = E_Class;
				}
				else if (strcmp(pomoc,"continue") == 0)
				{
					token->typ = E_Continue;
				}
				else if (strcmp(pomoc,"do") == 0)
				{
					token->typ = E_Do;
				}
				else if (strcmp(pomoc,"double") == 0)
				{
					token->typ = E_Double;
				}
				else if (strcmp(pomoc,"else") == 0)
				{
					token->typ = E_Else;
				}
				else if (strcmp(pomoc,"false") == 0)
				{
					token->typ = E_False;
				}
				else if (strcmp(pomoc,"for") == 0)
				{
					token->typ = E_For;
				}
				else if (strcmp(pomoc,"if") == 0)
				{
					token->typ = E_If;
				}
				else if (strcmp(pomoc,"int") == 0)
				{
					token->typ = E_Int;
				}
				else if (strcmp(pomoc,"return") == 0)
				{
					token->typ = E_Return;
				}
				else if (strcmp(pomoc,"String") == 0)
				{
					token->typ = E_String;
				}
				else if (strcmp(pomoc,"static") == 0)
				{
					token->typ = E_Static;
				}
				else if (strcmp(pomoc,"true") == 0)
				{
					token->typ = E_True;
				}
				else if (strcmp(pomoc,"void") == 0)
				{
					token->typ = E_Void;
				}
				else if (strcmp(pomoc,"while") == 0)
				{
					token->typ = E_While;
				}
				//pokud neni klicove slovo potrebujeme veden nazev
				else
				{
					
					token->value.s = pomoc;		
					printf("token hodnota %s \n", token->value.s);
				}
				znak = fgetc(s_file);
				return returnCode;
				break;

			}
			case 'c':		//cislovy literal
			{	
				token->typ = E_Int_Literal;
				int count = 1;
				znak = fgetc(s_file);
				//dokud je to cislice, pricitej po jedny do count
				while (isdigit(znak))		 
				{
					znak = fgetc(s_file);
					count++;
				}
				//pokud dalsi znak je exponent, pricte jedna ke count, typ je double
				if ((znak == 'e') || (znak == 'E'))	
				{
					token->typ = E_Double_Literal;
					znak = fgetc(s_file);
					count++;
					//pokud je v cisle + nebo - , pricte jedna ke count
					if ((znak == '+') || (znak == '-'))
					{
						znak = fgetc(s_file);
						count++;
					}
					//pokud po exponentu nebo po +/- neni cislo, je to lexikalni chyba
					if (!isdigit(znak))
					{
						returnCode = RC_LexicalErr;
						return returnCode;
						break;
					}
					//pokud je cislo
					else
					{
						//dokud je to cislice, pricitej po jedny do count
						while (isdigit(znak))
						{
							znak = fgetc(s_file);
							count++;
						}
						count++;
						pomoc = malloc(count * sizeof(char)); //alokace promenne pomoc podle hodnoty v count
						//pokud se alokace nezdarila
						if (pomoc == NULL)
						{
							returnCode = RC_InternalErr;
							return returnCode;
							break;
						}
						fseek(s_file, -count, SEEK_CUR);		//vraceni v souboru na pozici zacatku cisla
						for (i = 0; i < count-1; i++)		//nacteni do pomoc cislo
							pomoc[i] = fgetc(s_file);
						pomoc[i] = '\0';
						doublePomoc = strtod(pomoc, NULL);		//prevod do double
						free(pomoc);						
						token->value.d = doublePomoc;			//ulozeni EXPONENTU
						znak = fgetc(s_file);						
						return returnCode;						
						break;
					}
				}
				//pokud dalsi znak je tecka, pricte jedna ke count, typ je double 
				if (znak == '.')			//DESETINNA CAST
				{
					token->typ = E_Double_Literal;
					count++;
					znak = fgetc(s_file);
					//pokud po tecce neni cislo, je to lexikalni chyba
					if (!isdigit(znak))
					{
						returnCode = RC_LexicalErr;
						return returnCode;
						break;
					}
					//dokud je to cislice, pricitej po jedny do count
					while (isdigit(znak))
					{
						count++;
						znak = fgetc(s_file);
					}
					//pokud dalsi znak je exponent, pricte jedna ke count, 
					if ((znak == 'e') || (znak == 'E'))		//DESETINNA CAST + EXPONENT
					{
						znak = fgetc(s_file);
						count++;
						//pokud je v cisle + nebo - , pricte jedna ke count
						if ((znak == '+') || (znak == '-'))
						{
							znak = fgetc(s_file);
							count++;
						}
						//pokud po exponentu nebo po +/- neni cislo, je to lexikalni chyba
						if (!isdigit(znak))
						{
							returnCode = RC_LexicalErr;
							return returnCode;						
							break;
						}
						//pokud je cislo
						else
						{
							//dokud je to cislice, pricitej po jedny do count
							while (isdigit(znak))
							{
								znak = fgetc(s_file);
								count++;
							}
							count++;
							pomoc = malloc(count * sizeof(char));	//alokace promenne pomoc podle hodnoty v count
							//pokud se alokace nezdarila
							if (pomoc == NULL)
							{
								returnCode = RC_InternalErr;
								return returnCode;
								break;
							}
							fseek(s_file, -count, SEEK_CUR); //vraceni v souboru na pozici zacatku cisla
							for (i = 0; i < count-1; i++) //nacteni do pomoc cislo
								pomoc[i] = fgetc(s_file);
							pomoc[i] = '\0';
							doublePomoc = strtod(pomoc, NULL);		//prevod do double
							free(pomoc);							
							token->value.d = doublePomoc;			//ulozeni DESSETINNE CASTI A EXPONENTU
							znak = fgetc(s_file);
							return returnCode;							
							break;
						}
					}
					//pokud je cislo jen s desetinnou casti bez exponentu
					count++;
					pomoc = malloc(count * sizeof(char)); //alokace promenne pomoc podle hodnoty v count
					//pokud se alokace nezdarila
					if (pomoc == NULL)
					{
						returnCode = RC_InternalErr;
						return returnCode;
						break;
					}
					fseek(s_file, -count, SEEK_CUR);	//vraceni v souboru na pozici zacatku cisla
					for (i = 0; i < count-1; i++)	//nacteni do pomoc cislo
						pomoc[i] = fgetc(s_file);
					pomoc[i] = '\0';
					doublePomoc = strtod(pomoc,NULL);		//prevod do double
					free(pomoc);
					token->value.d = doublePomoc;			//ulozeni DESSETINNE CASTI
					znak = fgetc(s_file);					
					return returnCode;
					break;
				}
				//pokud je obycejne cislo
				count++;
				pomoc = malloc(count * sizeof(char)); //alokace promenne pomoc podle hodnoty v count
				//pokud se alokace nezdarila
				if (pomoc == NULL)
				{
					returnCode = RC_InternalErr;
					return returnCode;
					break;
				}
				
				fseek(s_file, -count, SEEK_CUR);
				for (i = 0; i < count-1; i++) //nacteni do pomoc cislo
					pomoc[i] = fgetc(s_file);
				pomoc[i] = '\0';			//vytvoreni stringu
				integerPomoc = atoi(pomoc);	//prevod do int
				free(pomoc);				
				token->value.i = integerPomoc;		//ulozeni obycejneho cisla
				znak = fgetc(s_file);				
				return returnCode;
				
				break;				
			}
			case 'r':		//retezcovy literal
			{
				int konec = 0;
				int count = 1;
				znak = fgetc(s_file);
				//zacatek nacitani retezce, dokud se nenajdou druhe uvozovky
				while (konec != 1)
				{
					//osetreni zda neni escape sekvence \"
					if (znak == '\\')
					{
						znak = fgetc(s_file);
						count++;
						if (znak == '"')
						{
							znak = fgetc(s_file);
							count++;
						}
					}
					//pokud jsou posledni uvozovky, nebo konec radku
					else if ((znak == '"' ) || (znak == '\n'))
					{
					//pokud je konec radku je to lexikalni chyba
					if (znak == '\n')
						{
							returnCode = RC_LexicalErr;
							return returnCode;						
						}
						znak = fgetc(s_file);
						konec = 1;
					}
					//pokud nejsou posledni uvozovky nacita se po znaku a zvysuje se promenna count
					else
					{
						count++;
						znak = fgetc(s_file);
					}
				}
				pomoc = malloc(count * sizeof(char)); //alokace promenne pomoc podle hodnoty v count
				//pokud se alokace nezdarila
				if (pomoc == NULL)
				{
					returnCode = RC_InternalErr;
					return returnCode;
					break;
				}
				fseek(s_file, -count -1, SEEK_CUR);	//vraceni v souboru na pozici zacatku retezce
				for (i = 0; i < count - 1; i++)	
					pomoc[i] = fgetc(s_file); 	//nacteni do pomoc retezec
				pomoc[i] = '\0';				//vytvoreni stringu
				//zacatek parsovani retezce
				int r;
				char asciiH[4];				//pomocny retezec pro octalovou sekvenci
				char final[strlen(pomoc)];
				final[0] = '\0';
				//parsovani po jednotlivych znacich
				for(r = 0;r < strlen(pomoc);r++)
				{
					long decim = 0;
					long octa = 0;
					int digit = 0;
					int position = 0;
					char prevod[2];		//pomocny retezec
					prevod[0] = pomoc[r];
					prevod[1] = '\0';
					//pokud je znak '\' a dale musi byt znak pro escape sekvenci
					if ((pomoc[r] == '\\') && (r<strlen(pomoc) - 1))
					{
						//escape sekvence \"
						 if (pomoc[r+1] == '"')
						 {
							 prevod[0] = '"';
							 r++;
						 }
						 //escape sekvence \n
						 if (pomoc[r+1] == 'n')
						 {
							 prevod[0] = '\n';
							 r++;
						 }
						 //escape sekvence \t
						 if (pomoc[r+1] == 't')
						 {
							 prevod[0] = '\t';
							 r++;
						 }
						 //escape sekvence '\\'
						 if (pomoc[r+1] == '\\')
						 {
							 prevod[0] = '\\';
							 r++;
						 }
					}
					//pokud je znak '\' a dale musi byt cislo
					if ((pomoc[r] == '\\') && (r<strlen(pomoc) - 3))
						//pokud nasledujici 3 znaky jsou cislo nacte se cele cislo do retezce
						if ((isdigit(pomoc[r+1])) && (isdigit(pomoc[r+2])) && (isdigit(pomoc[r+3])))
						{
							
							asciiH[0] = pomoc[r+1];
							asciiH[1] = pomoc[r+2];
							asciiH[2] = pomoc[r+3];
							asciiH[3] = '\0';
							octa = atol(asciiH);	//prevod retezce do int v osmickovem tvaru
							//overeni zda cislo neni vetsi nez 377
							if (octa < 378)
							{
								//prevod osmickoveho cisla do desitkoveho
								while(octa!=0) 
								{   
									digit = octa%10;
									decim += digit*pow(8, position); 
									position++;  
									octa /= 10;  
								}  
								prevod[0] = decim;
								prevod[1] = '\0';
								r=r+3;
							}
						}
					strcat(final,prevod);
				}
				free(pomoc);
				token->value.s = final;		//ulozeni vysledneho retezce
				token->typ = E_String_Literal;
				znak = fgetc(s_file);
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'p':		//plus
			{		
				token->typ = E_Plus;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'm':		//minus
			{
				token->typ = E_Minus;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'n':		//nasobeni
			{
				token->typ = E_Multiple;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'd':		//deleni, koment
			{
				znak = fgetc(s_file);
				//radkovy komentar
				if (znak == '/')
				{
					while (znak != '\n')
					{
						znak = fgetc(s_file);
					}
					stav = 's';
					break;
				}
				//blokovy komentar
				else if (znak == '*')		
				{
					znak = fgetc(s_file);
					while (1)
					{
						znak = fgetc(s_file);
						if (znak == '*')
						{	
							znak = fgetc(s_file);
							if (znak == '/')
							{
								znak = fgetc(s_file);
								stav = 's';
								break;
							}
						}
						if (znak == EOF)
						{
							returnCode = RC_LexicalErr;
							return returnCode;	
						}

					}
				break;
				}
				else
					token->typ = E_Divide;
				return returnCode;
				break;
			}
			case 'l':		//lesser, mensi
			{
				token->typ = E_Lesser;
				znak = fgetc(s_file);
				//mensi nebo rovnase
				if (znak == '=')
				{
					token->typ = E_Lesser_Equal;
					znak = fgetc(s_file);
				}
				return returnCode;
				break;
			}
			case 'g':		//greater, vetsi
			{
				token->typ = E_Greater;
				znak = fgetc(s_file);
				//vetsi nebo rovnase
				if (znak == '=')
				{
					token->typ = E_Greater_Equal;
					znak = fgetc(s_file);
				}
				return returnCode;
				break;
			}
			case 'a':		//assigment, prirazeni
			{
				token->typ = E_Assigment;
				znak = fgetc(s_file);
				//rovna se
				if (znak == '=')
				{
					token->typ = E_Equal;
					znak = fgetc(s_file);
				}
				return returnCode;
				break;
			}
			case 'q':		//strednik
			{
				token->typ = E_Semicolon;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'w':		//carka
			{
				token->typ = E_Comma;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'v':		//vykricnik
			{
				token->typ = 0;
				znak = fgetc(s_file);
				//nerovna se
				if (znak == '=')
				{
					token->typ = E_Not_Equal;
					znak = fgetc(s_file);
					return returnCode;
				}
				returnCode = RC_LexicalErr;
				return returnCode;
				break;
			}
			case 'b':		//leva zavorka
			{
				token->typ = E_Left_Bracket;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'x':		//prava zavorka
			{
				token->typ = E_Right_Bracket;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'h':		//leva slozena zavorka
			{
				token->typ = E_Left_Curly_Bracket;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}
			case 'k':		//prava slozena zavorka
			{
				token->typ = E_Right_Curly_Bracket;
				znak = fgetc(s_file);
				return returnCode;
				break;
			}

		}
	}
	token->typ = E_EOF;
	znak = fgetc(s_file);
	return returnCode;
}