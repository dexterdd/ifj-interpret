#include <stdbool.h>
#include <string.h>
#include "error.h"
#include "ial.h"
#include "ifj16.h"

#ifndef INTERPRET_H
#define INTERPRET_H
#define ALLOC_STEP 10

/* INTERPRETER */
int runInterpreter();
/* INTERPRETER END */

#endif
