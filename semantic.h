#ifndef SEMANTIC_H
#define SEMANTIC_H

#include <stdbool.h>
#include "lexical.h"
#include "ial.h"

#define PT_DEBUG true

typedef enum{
    PT_OK,
    PT_Err,
    PT_InErr
} tError;

typedef enum{
    L,  //< mensi
    G,  //> vetsi
    E,  //= rovno
    N   //prazdne misto v tabulce -> synt. chyba
} tRule;

typedef enum{
    I_Add,          //0 +
    I_Sub,          //1 -
    I_Mul,          //2 *
    I_Div,          //3 /
    I_Equal,        //4 ==
    I_NotEqual,     //5 !=
    I_LessOrEq,     //6 <=
    I_GreatOrEq,    //7 >=
    I_Greater,      //8 >
    I_Lesser,       //9 <
    I_LeftBr,       //10 (
    I_RightBr,      //11 )
    I_Id,           //12 promenna a konstanta
    I_Function,     //13 volani funkce
    I_Comma,        //14 ,
    I_End,          //15 $ konec vyrazu
    I_L,            //16 oznaceni < na zasobniku
    I_X,            //17 neterminalni znak napr. E->
    I_Error         //18 jiny token do vyrazu nepripoustime
} tIndex;

typedef struct{
    char* symbol;   //polozka tabulky symbolu
    tIndex idx;     //pseudopolozky s kterymi budu operovat na zasobniku
} tItem;

// =================================
// | zasobnik pro postfix          |
// | obsahuje ukazatele na tokeny  |
// =================================
    #define MAX_STACK 1024

    typedef struct sElem tElem;
    struct sElem {
        tItem* item;
        tElem* lptr;
    };

    typedef struct {
        tElem* top;
    } tPTstack;


    void PTSinit        ( tPTstack* stack );
    bool PTSempty       ( tPTstack* stack );
    tItem* PTStop       ( tPTstack* stack );
    void PTSpop         ( tPTstack* stack );
    void PTSpush        ( tPTstack* stack, tItem* item );
    tItem* PTStopPop    ( tPTstack* stack );
// ================================


int PT_Error(int error);

// rizeni semanticke analyzy
int expr(bool pure, char* dest, char** logicRes);
// zpracuje token
int utilizeToken(tToken token, tPTstack* stack, tPTstack* nonTermStack);
// prevede typ tokenu na index precedencni tabulky
tItem* getIndex(tToken* token);
// vyhodnoti pravidlo z precedencni tabulky
tRule getRule(tItem* term, tPTstack* stack, tPTstack* nonTermStack);
// provede akce v zavislosti na pravidle
int useRule(tRule rule, tItem* term, tPTstack* stack, tPTstack* nonTermStack);

// generuje unikatni idetifikator pro neterminal (I_X)
char* generateId();
// generuje instrukci
char* generOpInstruct(tIndex idx, tItem* nonTerm1, tItem* nonTerm2);
// zjisti zda je identifikator funkce
    int addArg(tItem* term, tPTstack* nonTermStack, int counter);
// zkontroluje zda ma promenna prirazenou hodnotu
bool isDefined(char* id);
// pretypuje promennou
    int nonTermRetype(tPTstack* nonTermStack);

// vlozi promennou do tabulky symbolu
    void addVar(int type, char* id, void* value);
// vlozi funkci do tabulky symbolu
    void addFunction(int type, char* id);



void funcTest();
void exprTest();

// pokud v zavorkach -> kontrola zda jako prvni je "("

#endif 