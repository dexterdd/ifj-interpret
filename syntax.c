/*
IFJ-Interpret jazyka IFJ16
syntax.c
Syntax analýza + semanticke akce

Varianta: b/3/I
Tým 093
   Mikulík Petr.....xmikul57
   Matuš Adam.......xmatus31
   Měrka Filip......xmerka04
   Svoboda David....xsvobo0t
*/

#include "error.h"
#include "ial.h"
#include "lexical.h"
#include "syntax.h"

/* 	LL Gramatika pro IFJ16
	Kazdy neterminal na leve strane ma svoji funkci
	Obecne by melo platit, ze se dalsi token nacita az kdyz je to nutne, tedy
	tesne pred vyhodnocovanim terminalu (napr pred switch(token->typ), pripadne uvnitr pravidla 
	pred testovanim dalsiho terminalu)

	<program> 			-> <classList> EOF

	<classList> 		-> ε
	<classList> 		-> class id { <classBody> } <classList>

	<classBody> 		-> ε
	<classBody>			-> static type id <definition> <classBody>

	<definition>		-> ;
	<definition>		-> = <expr> ;
	<definition>		-> ( <paramDef> ) { <funcBody> }

	<paramDef>			-> ε
	<paramDef>			-> type id <paramDefList>

	<paramDefList>		-> ε
	<paramDefList>		-> , type id <paramDefList>

	<funcBody>			-> ε
	<funcBody>			-> type id <varDef> <funcBody>
	<funcBody>			-> if ( <expr> ) { <statementList> } else { <statementList> } <funcBody>
	<funcBody>			-> while ( <expr> ) { <statementList> } <funcBody>
	<funcBody>			-> id <assign> ; <funcBody>
	<funcBody>			-> return <expr> ; <funcBody>

	<varDef>			-> = <expr> ;
	<varDef>			-> ;

	<statementList> 	-> ε
	<statementList> 	-> if ( <expr> ) { <statementList> } else { <statementList> } <statementList>
	<statementList> 	-> while ( <expr> ) { <statementList> } <statementList>
	<statementList> 	-> id <assign> ; <statementList>
	<statementList> 	-> return <expr> ; <statementList>

	<assign>			-> ( <param> )
	<assign>			-> = <expr>

	<param>				-> ε 	
	<param>				-> id <paramList>

	<paramList>			-> ε
	<paramList>			-> , id <paramList>	

	<expr>				-> #metoda vyhodnocovani vyrazu
*/

/*
	funkce se pokusi vlozit nazev tridy, pokud uz existuje, je to semantic error
*/
int insertClassId(char *cid)
{	
	for (int i = 0; i < classIdTop; i++) {
		if(strcmp(cid, classId[i]) == 0)
			return RC_SemDefinitionErr; // trida nalezena, nasleduje semantic error
	}
	classId[classIdTop] = cid;
	classIdTop++;

	return RC_OK;
}

symbolType getSymbolType(Token_type type)
{
	switch(type)
	{
		case E_Int: return T_INT;
		break;
		case E_Double: return T_DOUBLE;
		break;
		case E_Boolean: return T_BOOL;
		break;
		case E_Void: return T_VOID;
		break;
		case E_String: return T_STRING;
		break;
		default: return -1;
		break;
	}
}

int expr(symbolType expectedType)
{
	int result;

	result = get_token(token); if (result != RC_OK) return result;
	// SIMULACE ZPRACOVANI 1 TOKENU VYRAZU (napr. 5 nebo 5.4 nebo "ahoj")

	result = get_token(token); if (result != RC_OK) return result;
	// Simulace ")" nebo ";" nebo "," (tedy vyraz je v tomto stavu v poradku a dalsi token patri volajici funkci)

	return RC_OK;
}

int paramList()
{
	int result;

	// vyjimecne se nenacita dalsi protoze expr nam uz token dala
	//result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		// <paramList> -> , <expr> <paramList>	
		case E_Comma:

			result = expr(currentType);
			if (result != RC_OK) return result;

			result = paramList();
			if (result != RC_OK) return result;

			return RC_OK;

		break;


		// <paramList> -> ε 
		default:
			return RC_OK;
	}

	return RC_SyntaxErr;
}

int param()
{
	int result;

	// <param> -> <expr> <paramList>

	result = expr(currentType);
	if (result != RC_OK) return result;

	// TODO: vymyslet jak lepe zjistit konec parametrove casti pri volani funkce
	//if (token->typ == E_Right_Bracket) return RC_OK;

	result = paramList();
	if (result != RC_OK) return result;

	return RC_OK;
	
}


int assign()
{
	int result;
	tSymbol *resultSymbol;

	result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		// <assign>	-> ( <param> )
		case E_Left_Bracket:

			resultSymbol = getFunction(currentIdentif);
			if (resultSymbol == NULL) return RC_SemDefinitionErr; // funkce nebyla drive definovana

			result =  param(); // param() konci s nevyuzitym tokenem
			if (result != RC_OK) return result;

			if (token->typ != E_Right_Bracket) return RC_SyntaxErr;

			return RC_OK;

		break;

		// <assign> -> = <expr>
		case E_Assigment:

			resultSymbol = getSymbol(currentIdentif);
			if (resultSymbol == NULL) return RC_SemDefinitionErr; // promenna nebyla drive definovana

			result = expr(currentType);
			if (result != RC_OK) return result;

			tokenReady = 1;
			return RC_OK;

		break;


		// unexpected token
		default:
			return RC_SyntaxErr;
	}

	return RC_SyntaxErr;
}

int statementList()
{
	int result;

	result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		// <statementList> -> if ( <expr> ) { <statementList> } else { <statementList> } <statementList>
		case E_If:
			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Bracket) return RC_SyntaxErr;

			result = expr(T_BOOL);
			if (result != RC_OK) return result;

			// ZKONTROLOVAT !! expr() by mel potencionalne skoncit s nevyuzitym tokenem, proto se zde nenacita novy

			if (token->typ != E_Right_Bracket) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Curly_Bracket) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			// statementList() pouzil epsilon, token je aktualni

			if (token->typ != E_Right_Curly_Bracket) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Else) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Curly_Bracket) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			// statementList() pouzil epsilon, token je aktualni

			if (token->typ != E_Right_Curly_Bracket) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		// <statementList> -> while ( <expr> ) { <statementList> } <statementList>
		case E_While:
			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Bracket) return RC_SyntaxErr;

			result = expr(T_BOOL);
			if (result != RC_OK) return result;

			// ZKONTROLOVAT !! expr() by mel potencionalne skoncit s nevyuzitym tokenem, proto se zde nenacita novy

			if (token->typ != E_Right_Bracket) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Curly_Bracket) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			// statementList() pouzil epsilon, token je aktualni

			if (token->typ != E_Right_Curly_Bracket) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		// <statementList> -> id <assign> ; <statementList>
		case E_Identif:

			result = assign();
			if (result != RC_OK) return result;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Semicolon) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		// <statementList> -> return <expr> ; <statementList>
		case E_Return:

			result = expr(currentFuncType);
			if (result != RC_OK) return result;

			// ZKONTROLOVAT !! expr() by mel potencionalne skoncit s nevyuzitym tokenem, proto se zde nenacita novy

			if (token->typ != E_Semicolon) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			return RC_OK;

		break;


		// <statementList> -> ε
		default:
			return RC_OK;
	}

	return RC_SyntaxErr;
}

int varDef()
{
	int result;
	tSymbol *resultSymbol;

	result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		//<varDef> -> = <expr> ;
		case E_Assigment:

			result = expr(currentType);
			if (result != RC_OK) return result;

			// ZKONTROLOVAT !! expr() by mel potencionalne skoncit s nevyuzitym tokenem, proto se zde nenacita novy

			if (token->typ != E_Semicolon) return RC_SyntaxErr;

			return RC_OK;

		break;

		//<varDef> -> ;
		case E_Semicolon:

			resultSymbol = getSymbol(currentIdentif);
			if (resultSymbol != NULL) return RC_SemDefinitionErr;

			switch(currentType)
			{
				case T_INT: resultSymbol = insertIntNoInit(currentIdentif); break;
				case T_STRING: resultSymbol = insertStringNoInit(currentIdentif); break;
				case T_BOOL: resultSymbol = insertBoolNoInit(currentIdentif); break;
				case T_DOUBLE: resultSymbol = insertDoubleNoInit(currentIdentif); break;
				case T_VOID: return RC_SemOtherErr; // void muze byt jenom funkce
				default: return RC_SemOtherErr;
			}

			return RC_OK;
		break;


		// unexpected token
		default:
			return RC_SyntaxErr;
	}

	return RC_SyntaxErr;
}

int funcBody()
{
	int result;
	//tSymbol *resultSymbol;

	result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		// <funcBody> -> type id <varDef> <funcBody>	
		case E_Int:
		case E_Double:
		case E_String:
		case E_Boolean:
		case E_Void:
			currentType = getSymbolType(token->typ);

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Identif) return RC_SyntaxErr;
			currentIdentif = token->value.s;

			result = varDef();
			if (result != RC_OK) return result;

			result = funcBody();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		// <funcBody> -> if ( <expr> ) { <statementList> } else { <statementList> } <funcBody>
		case E_If:
			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Bracket) return RC_SyntaxErr;

			result = expr(T_BOOL);
			if (result != RC_OK) return result;

			// ZKONTROLOVAT !! expr() by mel potencionalne skoncit s nevyuzitym tokenem, proto se zde nenacita novy

			if (token->typ != E_Right_Bracket) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Curly_Bracket) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			// statementList() pouzil epsilon, token je aktualni

			if (token->typ != E_Right_Curly_Bracket) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Else) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Curly_Bracket) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			// statementList() pouzil epsilon, token je aktualni

			if (token->typ != E_Right_Curly_Bracket) return RC_SyntaxErr;

			result = funcBody();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		//<funcBody> -> while ( <expr> ) { <statementList> } <funcBody>
		case E_While:
			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Bracket) return RC_SyntaxErr;

			result = expr(T_BOOL);
			if (result != RC_OK) return result;

			// ZKONTROLOVAT !! expr() by mel potencionalne skoncit s nevyuzitym tokenem, proto se zde nenacita novy

			if (token->typ != E_Right_Bracket) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Curly_Bracket) return RC_SyntaxErr;

			result = statementList();
			if (result != RC_OK) return result;

			// statementList() pouzil epsilon, token je aktualni

			if (token->typ != E_Right_Curly_Bracket) return RC_SyntaxErr;

			result = funcBody();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		//<funcBody> -> id <assign> ; <funcBody>
		case E_Identif:
			currentIdentif = token->value.s;

			result = assign();
			if (result != RC_OK) return result;

			if (!tokenReady) {result = get_token(token); if (result != RC_OK) return result;}

			if (token->typ != E_Semicolon) return RC_SyntaxErr;

			result = funcBody();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		//<funcBody> -> return <expr> ; <funcBody>
		case E_Return:

			result = expr(currentFuncType);
			if (result != RC_OK) return result;

			// ZKONTROLOVAT !! expr() by mel potencionalne skoncit s nevyuzitym tokenem, proto se zde nenacita novy

			if (token->typ != E_Semicolon) return RC_SyntaxErr;

			result = funcBody();
			if (result != RC_OK) return result;

			return RC_OK;			

		break;

		//<funcBody> -> ε
		default: 
			return RC_OK;
	}

	return RC_SyntaxErr;
}

int paramDefList()
{
	int result;
	tSymbol *resultSymbol;

	result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		// <paramDefList> -> , type id <paramDefList>
		case E_Comma:
			result = get_token(token); if (result != RC_OK) return result;

			if (!(token->typ == E_Int || token->typ == E_Double || token->typ == E_String || token->typ == E_Boolean || token->typ == E_Void)) {
				return RC_SyntaxErr;
			}
			currentType = getSymbolType(token->typ);

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Identif) return RC_SyntaxErr;

			currentIdentif = token->value.s;
			resultSymbol = addArgument(currentFuncId, currentIdentif, currentType);
			if (resultSymbol == NULL) return RC_InternalErr;
			resultSymbol = getSymbol(currentIdentif);
			if (resultSymbol != NULL) return RC_SemDefinitionErr;
			switch(currentType)
			{
				case T_INT: resultSymbol = insertIntNoInit(currentIdentif); break;
				case T_STRING: resultSymbol = insertStringNoInit(currentIdentif); break;
				case T_BOOL: resultSymbol = insertBoolNoInit(currentIdentif); break;
				case T_DOUBLE: resultSymbol = insertDoubleNoInit(currentIdentif); break;
				case T_VOID: return RC_SemOtherErr; // void muze byt jenom funkce
				default: return RC_SemOtherErr;
			}

			result = paramDefList();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		// <paramDefList> -> ε
		default:
			return RC_OK;
	}

	return RC_SyntaxErr;
}

int paramDef()
{
	int result;
	tSymbol *resultSymbol;

	result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		// <paramDef> -> type id <paramDefList>
		case E_Int:
		case E_Double:
		case E_String:
		case E_Boolean: 
		case E_Void:

			currentType = getSymbolType(token->typ);

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Identif) return RC_SyntaxErr;

			currentIdentif = token->value.s;
			resultSymbol = addArgument(currentFuncId, currentIdentif, currentType);
			if (resultSymbol == NULL) return RC_InternalErr;
			resultSymbol = getSymbol(currentIdentif);
			if (resultSymbol != NULL) return RC_SemDefinitionErr;
			switch(currentType)
			{
				case T_INT: resultSymbol = insertIntNoInit(currentIdentif); break;
				case T_STRING: resultSymbol = insertStringNoInit(currentIdentif); break;
				case T_BOOL: resultSymbol = insertBoolNoInit(currentIdentif); break;
				case T_DOUBLE: resultSymbol = insertDoubleNoInit(currentIdentif); break;
				case T_VOID: return RC_SemOtherErr; // void muze byt jenom funkce
				default: return RC_SemOtherErr;
			}

			result = paramDefList();
			if (result != RC_OK) return result;

			return RC_OK;

		break; 

		// <paramDef> -> ε
		default:
			return RC_OK;
	}

	return RC_SyntaxErr;
}

int definition()
{
	int result;
	tSymbol *resultSymbol;

	result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		// <definition>	-> ;
		case E_Semicolon:

			resultSymbol = getSymbol(currentIdentif);
			if (resultSymbol != NULL) return RC_SemDefinitionErr; // pokus o redefinici promenne

			switch(currentType)
			{
				case T_INT: resultSymbol = insertIntNoInit(currentIdentif); break;
				case T_STRING: resultSymbol = insertStringNoInit(currentIdentif); break;
				case T_BOOL: resultSymbol = insertBoolNoInit(currentIdentif); break;
				case T_DOUBLE: resultSymbol = insertDoubleNoInit(currentIdentif); break;
				case T_VOID: return RC_SemOtherErr; // void muze byt jenom funkce
				default: return RC_SemOtherErr;
			}

			return RC_OK;

		break;

		// <definition>	-> = <expr> ;
		case E_Assigment:

			resultSymbol = getSymbol(currentIdentif);
			if (resultSymbol != NULL) return RC_SemDefinitionErr; // pokus o redefinici promenne

			result = expr(currentType);
			if (result != RC_OK) return result;

			// ZKONTROLOVAT !! expr() by mel potencionalne skoncit s nevyuzitym tokenem, proto se zde nenacita novy

			if (token->typ != E_Semicolon) return RC_SyntaxErr;

			return RC_OK;

		break;

		// <definition>	-> ( <paramDef> ) { <funcBody> }
		case E_Left_Bracket:

			resultSymbol = getFunction(currentIdentif);
			if (resultSymbol != NULL) return RC_SemDefinitionErr; // pokus o redefinici promenne
			resultSymbol = insertFunction(currentIdentif, currentType);
			if (resultSymbol == NULL) return RC_InternalErr;
			currentFuncId = currentIdentif; // bude se pracovat s touto funkci
			currentFuncType = currentType; // funkce je tohoto typu (pro overovani return typu)

			addScopeLabel(currentFuncId); // zacatek scope pro funkci, definovane parametry budou uz lokalni promenne

			result = paramDef();
			if (result != RC_OK) return result;

			// paramDef nebo vnorena paramDefList pouzila epsilon, token je uz nacteny

			if (token->typ != E_Right_Bracket) return RC_SyntaxErr;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Curly_Bracket) return RC_SyntaxErr;

			result = funcBody();
			if (result != RC_OK) return result;
			// funcBody pouzila epsilon, token je uz nacteny

			removeLastScopeLabel(); // konec scope pro funkci

			if (token->typ != E_Right_Curly_Bracket) return RC_SyntaxErr;

			return RC_OK;

		break;

		// unexpected token
		default:
			return RC_SyntaxErr;
	}

	// unexpected token
	return RC_SyntaxErr;
}

int classBody()
{
	int result;

	result = get_token(token); if (result != RC_OK) return result;

	switch (token->typ) {
		// <classBody>	-> static type id <definition> <classBody>
		case E_Static:
			result = get_token(token); if (result != RC_OK) return result;

			// token musi byt jeden z techto E_Int, E_Double, E_String, E_Boolean, E_Void
			
			if (!(token->typ == E_Int || token->typ == E_Double || token->typ == E_String || token->typ == E_Boolean || token->typ == E_Void)) return RC_SyntaxErr;
			currentType = getSymbolType(token->typ);

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Identif) return RC_SyntaxErr;

			currentIdentif = token->value.s;

			result = definition();
			if (result != RC_OK) return result;

			result = classBody();
			if (result != RC_OK) return result;

			return RC_OK;

		break;

		// <classBody> 	-> ε
		default:
			return RC_OK;
	}

	return RC_SyntaxErr;
}


int classList()
{
	int result;

	result = get_token(token); 
	if (result != RC_OK) return result;

	switch (token->typ) {
		// <classList> 	-> class id { <classBody> } <classList>
		case E_Class:

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Identif) return RC_SyntaxErr;

			currentIdentif = token->value.s;
			result = insertClassId(currentIdentif);
			if (result != RC_OK) return result;
			result = addScopeLabel(currentIdentif); // zacatek scope pro tridu
			if (result != RC_OK) return result;

			result = get_token(token); if (result != RC_OK) return result;

			if (token->typ != E_Left_Curly_Bracket)
				return RC_SyntaxErr;

			result = classBody();
			if (result != RC_OK) return result;

			// classBody pouzila epsilon, token je uz nacteny

			if (token->typ != E_Right_Curly_Bracket)
				return RC_SyntaxErr;

			removeLastScopeLabel();	// konec scope pro tridu

			result = classList();
			if (result != RC_OK) return result;

			return RC_OK;

		break; // E_Class

		// <classBody> -> ε
		default: 
			return RC_OK;
	}

	// unexpected token
	return RC_SyntaxErr;
}

int program()
{
	int result;

	// <program> -> <classList> EOF
	
	result = classList();
	if (result != RC_OK) return result;

	// classList() pouzila epsilon, token je uz nacteny	

	// test konce vstupniho programu
	
	if (token->typ != E_EOF) return RC_SyntaxErr;

	tSymbol *mainRunS = getFunction("Main.Run");	// nasla se funkce Run v tride Main?
	if (mainRunS == NULL) return RC_SemDefinitionErr;
	if (mainRunS->isFunction == false) return RC_SemDefinitionErr;	// je to funkce
	if (mainRunS->isDefined == false) return RC_SemDefinitionErr;	// definovana (static ...)
	if (mainRunS->type != T_VOID) return RC_SemDefinitionErr;	// bez navratoveho typu (void)
	if (mainRunS->nextArg != NULL) return RC_SemDefinitionErr; // bezparametricka

	// -> vygenerovat instrukci konce programu
	addInstruction(I_HALT,NULL,NULL,NULL);

	return RC_OK;
}

int parse()
{
	int result;
	tokenReady = 0; // pouziva se jako signalizace pri vynorovani, jestli je dalsi token uz nacteny

	classIdTop = 1;
	classId[0] = "ifj16";

	token = create_token();

	
	result = program();


	destroy_token(token);
	//destroyToken(tmpTok);

	return result;
}

// KONEC SYNTAKTICKE ANALYZY

// TESTOVANI