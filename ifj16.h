#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "ial.h"
#include "error.h"

/* Ze standardního vstupu se nacte retezec ukoncený koncem rádku nebo koncem vstupu, kdy symbol konce rádku ci konce vstupu již do na cteného retezce nepatrí (tj. lze nacíst i prázdný retezec). readString nactený retezec vrátí jako výsledek. Funkce readInt a readDouble nactený retezec prevedou na práve jedno celé nebo desetinné císlo a vratí jej jako výsledek. */
int readBool(bool* b);
int readDouble(double*);
int readInt(int*);
void readString(char*);


/* s = term nebo kokatenace termu (term + ...)
   term = int, double, String
*/
void printBool(bool b);
void printDouble(double d);
void printInt(int i);
void print(char* s);
// void printTerms(tSymbol* symbols);

/* Vrátí délku (pocet znaku) retezce zadaného jediným parametrem s. */
int length(char* s);

/* Vrátí podˇretezec zadaného retezce s. Druhým parametrem i je dán zacátek požadovaného pod retezce (pocítáno od nuly) a tretí parametr n urcuje délku podretezce. */
char* substr(char* s, int i, int n);

/* Lexikograficky porovná dva zadané retezce s1 a s2 a vrátí celocíselnou hodnotu dle toho, zda je s1 pred, roven, nebo za s2. Jsou-li retezce s1 a s2 stejné, vrací 0; je-li s1 vetší než s2, vrací 1; jinak vrací -1. */
int compare(char* s1, char* s2);

/* Vyhledá první výskyt zadaného podretezce search v retezci s a vrátí jeho pozici (pocítáno od nuly). Prázdný retezec se vyskytuje v libovolném retezci na pozici 0. Pokud podˇretezec není nalezen, je vrácena hodnota -1. Pro vyhledání podˇretezce v retezci použijte metodu, která odpovídá vašemu zadání. */
int find(char* s, char* search);

/* Seradí znaky v daném retezci s tak, aby znak s nižší ordinální hodnotou vždy predcházel znaku s vyšší ordinální hodnotou. Vrácen je retezec obsahující se razené znaky. Pro razení použijte metodu, která odpovídá vašemu zadání. */
char* sort(char* s);

