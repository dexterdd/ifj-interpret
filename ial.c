#include "ial.h"



/* INSTRUCTION LIST */
int initInstructionList()
{
	list = malloc(sizeof(struct sInstructionList));
	if(list == NULL) return RC_InternalErr;
	
	list->first = NULL;
	list->last = NULL;
	list->active = NULL;
	
	return RC_OK;
}
tInstruction *createInstruction(instructionType type, void* out, void* in1, void* in2)
{
	tInstruction *i = malloc(sizeof(tInstruction));
	if(i!=NULL) {
		i->type = type;
		i->out = out;
		i->in1 = in1;
		i->in2 = in2;
	}
	return i;
}
tInstruction *addInstruction(instructionType type, void* out, void* in1, void* in2)
{
	if(list == NULL) return NULL;
	
	tInstruction *i = createInstruction(type, out, in1, in2);
	return addExistingInstruction(i);
}
tInstruction *addExistingInstruction(tInstruction *i)
{
	if(list == NULL || i == NULL) return NULL;
	
	if(list->first == NULL)
		list->first = i;
	else
		list->last->next = i;
		
	list->last = i;
	return i;
}
void nextInstruction()
{
	if(list->active == NULL)
		list->active = list->first;
	else
		list->active = list->active->next;
}
tInstruction *getActiveInstruction()
{
	return list->active;
}
tInstruction *getLastInstruction()
{
	return list->last;
}
int goToInstruction(tInstruction *to)
{
	tInstruction *i = list->first;
	
	while(i!=NULL){
		if(i == to){
			list->active = i;
			return RC_OK;
		}
		i = i->next;
	}
	return RC_InternalErr;
}
void printInstructions()
{
	tInstruction *i = list->first;
	int n = 0;
	const char* TYPES[] = {"NOP    ","SETC   ","SET    ","ADD    ","SUB    ","MUL    ","DIV    ","NOT    ","AND    ","OR     ","EQ     ","NOTEQ  ","LESS   ","MORE   ","LESSEQ ","MOREEQ ","INC    ","DEC    ","READ   ","WRITE  ","JMP    ","JMPT   ","JMPF   ","JMPfun ","JMPback","SETpar ","HALT   "};
	
	while(i!=NULL){
	
		printf("%s ",i == list->active ? ">>":"  ");
		
		if(i->type==I_JMP
		|| i->type==I_JMPT
		|| i->type==I_JMPF)
			printf("%3d# %s [%13d#] [%14p] [%14p]",n++,TYPES[i->type],getIDbyPtr(i->out),i->in1,i->in2);
		else if(i->type==I_SETC)
			printf("%3d# %s [%14s] [%14p] [%14d]",n++,TYPES[i->type],i->out!=NULL?(char*)i->out:"NULL",i->in1,(symbolType) i->in2);
		else
			printf("%3d# %s [%14s] [%14s] [%14s]",n++,TYPES[i->type],i->out!=NULL?(char*)i->out:"NULL",i->in1!=NULL?(char*)i->in1:"NULL",i->in2!=NULL?(char*)i->in2:"NULL");
			
		printf("%s\n",i == list->active ? " <<":"");
		i = i->next;
	}
	printf("\n");
}
int getIDbyPtr(tInstruction *wanted)
{
	tInstruction *i = list->first;
	int n = 0;
	while(i!=NULL){
		if(i == wanted) return n;
		n++;
		i = i->next;
	}
	return -1;
}
void freeInstructionList()
{
	if(list == NULL) return;
	
	tInstruction *i;
	i = list->first;
	while(i != NULL){
		list->first = i->next;
		free(i);
		i = list->first;
	}
	
	free(list);
}
/* INSTRUCTION LIST END */



/* SCOPE */
int initScopeList()
{
	gsl = malloc(sizeof(struct sScopeList));
	if(gsl == NULL) return RC_InternalErr;
	
	gsl->first = NULL;
	gsl->last = NULL;
	
	return RC_OK;
}
int addScopeLabel(char* l)
{
	if(gsl == NULL
	|| l == NULL) return RC_InternalErr;
	
	tScopeItem *si = malloc(sizeof(tScopeItem));
	if(si == NULL) return RC_InternalErr;
	
	si->label = l;
	
	if(gsl->first == NULL)
		gsl->first = si;
	else	
		gsl->last->next = si;
	si->prev = gsl->last;
	si->next = NULL;
	gsl->last = si;
	
	return RC_OK;
}
char* generateSymbolName(char* n)
{
	if(gsl == NULL
	|| n == NULL) return NULL;
	
	tScopeItem *si = gsl->first;
	
	if(si != NULL){
		int length = strlen(n);	// delka jmena promenne
		char *gn;
		while(si != NULL){
			length += strlen(si->label) +1;
			si = si->next;
		}
		
		gn = malloc(length+1);
		if(gn == NULL) return NULL;
		strcpy(gn, "\0");	// kvuli podivnemu pametovemu efektu strcat();
		
		si = gsl->first;
		while(si != NULL){
			strcat(gn, si->label);
			strcat(gn, ".");
			si = si->next;
		}
		strcat(gn, n);
		strcat(gn, "\0");
		return gn;
	}
	
	return n;
}
void removeLastScopeLabel()
{
	if(gsl == NULL
	|| gsl->last == NULL) return;
	
	if(gsl->first == gsl->last){
		free(gsl->last);
		gsl->first = NULL;
		gsl->last = NULL;
	} else {
		gsl->last = gsl->last->prev;
		free(gsl->last->next);
		gsl->last->next = NULL;
	}
}
void clearScope()
{
	if(gsl == NULL) return;
	
	while(gsl->last != NULL)
		removeLastScopeLabel();
}
void freeScopeList()
{
	clearScope();
	if(gsl != NULL)
		free(gsl);
}



int initLocalScopeList(tScopeListPtr *lsl)
{
	*lsl = malloc(sizeof(struct sScopeList));
	if(*lsl == NULL) return RC_InternalErr;
	
	(*lsl)->first = NULL;
	(*lsl)->last = NULL;
	
	return RC_OK;
}
int copyGlobalScopeList(tScopeListPtr *lsl)
{
	if(gsl == NULL
	|| *lsl == NULL) return RC_InternalErr;
	
	if(gsl->first == NULL){
		(*lsl)->first = NULL;
		(*lsl)->last = NULL;
		return RC_OK;
	}
	
	tScopeItem *gsi = gsl->first;
	tScopeItem *lsi;
	while(gsi != NULL){								// pro kazkou polozku v globalnim seznamu
		lsi = malloc(sizeof(tScopeItem));			// vytvorit polozku
		if(lsi == NULL) return RC_InternalErr;
		
		lsi->label = malloc(strlen(gsi->label));	// zkopirovat label
		if(lsi->label == NULL){
			free(lsi);
			return RC_InternalErr;
		}
		strcpy(lsi->label, gsi->label);
		
		if((*lsl)->first == NULL)					// vlozit do lokalniho seznamu
			(*lsl)->first = lsi;
		else	
			(*lsl)->last->next = lsi;
		lsi->prev = (*lsl)->last;
		lsi->next = NULL;
		(*lsl)->last = lsi;
		
		gsi = gsi->next;							// dalsi polozka v globalnim seznamu
	}
	return RC_OK;
}
int setGlobalScopeList(tScopeListPtr *lsl)
{
	if(gsl == NULL
	|| *lsl == NULL) return RC_InternalErr;
	
	clearScope();								// vycistit globalni seznam
	
	if((*lsl)->first == NULL) return RC_OK;		// lokalni seznam je prazdny => hotovo
	
	tScopeItem *lsi  = (*lsl)->first;
	int rc;
	while(lsi != NULL){							// pro kazkou polozku v lokalnim seznamu
		rc = addScopeLabel(lsi->label);			// pridej label do globalniho seznamu	
		if(rc != RC_OK) return RC_InternalErr;
		
		lsi = lsi->next;						// dalsi polozka v lokalnim seznamu
	}
	return RC_OK;
}
void clearLocalScope(tScopeListPtr *lsl)
{
	if(*lsl == NULL) return;
	
	while((*lsl)->last != NULL){
		if((*lsl)->first == (*lsl)->last){
			if((*lsl)->last->label != NULL)
				free((*lsl)->last->label);
			free((*lsl)->last);
			(*lsl)->first = NULL;
			(*lsl)->last = NULL;
		} else {
			if((*lsl)->last->label != NULL)
				free((*lsl)->last->label);
			(*lsl)->last = (*lsl)->last->prev;
			free((*lsl)->last->next);
			(*lsl)->last->next = NULL;
		}
	}
	(*lsl)->first = NULL;
}
void freeLocalScopeList(tScopeListPtr *lsl)
{
	clearLocalScope(lsl);
	if(*lsl != NULL)
		free(*lsl);
}
/* SCOPE END */



/* JUMP STACK */
int initJumpList()
{
	jl = malloc(sizeof(struct sJumpList));
	if(jl == NULL) return RC_InternalErr;
	
	jl->first = NULL;
	jl->last = NULL;
	
	return RC_OK;
}
int addJump()
{
	if(jl == NULL) return RC_InternalErr;
	
	tJumpItem *ji = malloc(sizeof(tJumpItem));
	if(ji == NULL) return RC_InternalErr;
	
	if(jl->first == NULL)
		jl->first = ji;
	else
		jl->last->next = ji;
	ji->prev = jl->last;
	jl->last = ji;
	
	ji->from = getActiveInstruction();
	if(ji->from == NULL) return RC_InternalErr;
	
	int rc = initLocalScopeList( &ji->currScope );
	if(rc != RC_OK) return RC_InternalErr;
	
	rc = copyGlobalScopeList( &ji->currScope );
	if(rc != RC_OK) return RC_InternalErr;
	
	return RC_OK;
}
int getJump()
{
	if(jl == NULL
	|| jl->first == NULL) return RC_InternalErr;
	
	int rc = goToInstruction( jl->last->from );
	if(rc != RC_OK) return RC_InternalErr;
	
	rc = setGlobalScopeList( &jl->last->currScope );
	if(rc != RC_OK) return RC_InternalErr;
	
	return RC_OK;
}
void removeLastJump()
{
	if(jl == NULL) return;
	
	if(jl->first == jl->last){
		free(jl->last);
		jl->first = NULL;
		jl->last = NULL;
	} else {
		jl->last = jl->last->prev;
		free(jl->last->next);
		jl->last->next = NULL;
	}
}
void freeJumpList()
{
	if(jl == NULL) return;
	
	while(jl->last != NULL ){
		removeLastJump();
	}
	free(jl);
}
/* JUMP STACK END */



/* SYMBOL TABLE */
tSymbol *insertBool(char* key, bool b)
{
    tSymbol symbol;

    symbol.key = generateSymbolName(key);
    symbol.type = T_BOOL;
    symbol.value.b = b;
    symbol.isFunction = false;
    symbol.isDefined = true;
    symbol.nextArg = NULL;
    symbol.begin = NULL;

    return insertSymbol(symbol);
}
tSymbol *insertInt(char* key, int i)
{
    tSymbol symbol;

    symbol.key = generateSymbolName(key);
    symbol.type = T_INT;
    symbol.value.i = i;
    symbol.isFunction = false;
    symbol.isDefined = true;
    symbol.nextArg = NULL;
    symbol.begin = NULL;

    return insertSymbol(symbol);
}
tSymbol *insertDouble(char* key, double d)
{
    tSymbol symbol;

    symbol.key = generateSymbolName(key);
    symbol.type = T_DOUBLE;
    symbol.value.d = d;
    symbol.isFunction = false;
    symbol.isDefined = true;
    symbol.nextArg = NULL;
    symbol.begin = NULL;

    return insertSymbol(symbol);
}
tSymbol *insertString(char* key, char* s)
{
    tSymbol symbol;

    symbol.key = generateSymbolName(key);
    symbol.type = T_STRING;
    symbol.value.s = s;
    symbol.isFunction = false;
    symbol.isDefined = true;
    symbol.nextArg = NULL;
    symbol.begin = NULL;

    return insertSymbol(symbol);
}
tSymbol *insertSymbol(tSymbol symbol)
{
    if(symbol.key == NULL
    /*|| (symbol.type == T_VOID && !symbol.isFunction)*/
    ) return NULL;
   	
   	symbol.running = 0;
    tSymbolItemPtr sip;
    sip = insertIntoSymbolTable(&root, symbol);
	
    if (sip == NULL)
        return NULL;
    else
    	return &(sip->symbol);
}
tSymbol *insertFunction(char* key, symbolType type)
{
	if(key == NULL) return NULL;
	
	tSymbol symbol;

    symbol.key = generateSymbolName(key);
    if(symbol.key == NULL) return NULL;
    
    symbol.type = type;
    symbol.isFunction = true;
    symbol.isDefined = true;
    symbol.nextArg = NULL;
    symbol.begin = addInstruction(I_NOP, NULL,NULL,NULL);
    if(symbol.begin == NULL){
    	free(symbol.key);
    	return NULL;
    }
    
    tScopeListPtr lsl;
    if( initLocalScopeList(&lsl) != RC_OK	// ulozeni prislusneho scopu
    ||  copyGlobalScopeList(&lsl)!= RC_OK ){
    	free(symbol.key);
    	freeLocalScopeList(&lsl);
    	return NULL;
    }
    symbol.funcScope = lsl;
    
    tSymbol *rs = insertSymbol(symbol);
    if(rs == NULL){
    	free(symbol.key);
    	freeLocalScopeList(&lsl);
    }
    return rs;
}
tSymbol *addArgument(char* funcKey, char* argKey, symbolType argType)
{
	if(funcKey == NULL
	|| argKey == NULL) return NULL;
	
	tSymbol *s = getSymbol(funcKey);
	if(s==NULL
	|| !s->isFunction) return NULL;
	
	tSymbol *arg = s;
	while(arg->nextArg != NULL) arg = arg->nextArg;	// najdi posledni argument
	
	arg->nextArg = malloc(sizeof(tSymbol));
	if(arg->nextArg == NULL) return NULL;
	
	arg->nextArg->key = argKey;
	arg->nextArg->type = argType;
	arg->nextArg->nextArg = NULL;
	
	return arg->nextArg;
}
tSymbol *getFunction(char* key)
{	
	if(DEBUG) printf("Searching function %s\n", key);
	
	tSymbol *s = searchInSymbolTable(&root, key );	// primy zapis "Class.function()"
	if(DEBUG) printf("\t%s %s\n", key, s!=NULL?"<<":"");
	if(s!=NULL
	|| gsl->first == NULL) return s;
	
	int length = strlen(key);						// tridni funkce "function()"
	length += strlen(gsl->first->label) +1;
	
	char *gn = malloc(length);
	if(gn == NULL) return NULL;
	strcpy(gn, "\0");	// kvuli podivnemu pametovemu efektu strcat();
	
	strcat(gn, gsl->first->label);
	strcat(gn, ".");
	strcat(gn, key);
	s = searchInSymbolTable(&root, gn );
	if(DEBUG) printf("\t%s %s\n", gn, s!=NULL?"<<":"");
	free(gn);
	
	return s;
}
tSymbol *getSymbol(char* key)
{
    if(DEBUG) printf("Searching variable %s\n", key);
    
	char *gn = generateSymbolName(key);		// v soucasnem scopu
	tSymbol *s = searchInSymbolTable(&root, gn );
	if(DEBUG) printf("\t%s %s\n", gn, s!=NULL?"<<":"");
	if(gn != key) free(gn);
	if(s!=NULL) return s;
	
    if(gsl->first == NULL) return s;		// zadny scope label => hotovo
	if(gsl->first != gsl->last){	// alespon dva scope labely => test tridni var
    	int length = strlen(key);			// delka jmena promenne
		length += strlen(gsl->first->label) +1;
		
		gn = malloc(length);
		if(gn == NULL) return NULL;
		strcpy(gn, "\0");	// kvuli podivnemu pametovemu efektu strcat();
		
		strcat(gn, gsl->first->label);
		strcat(gn, ".");
		strcat(gn, key);
		s = searchInSymbolTable(&root, gn );
		if(DEBUG) printf("\t%s %s\n", gn, s!=NULL?"<<":"");
		free(gn);
		if(s != NULL) return s;
	}
	s = getFunction(key);	// navratova hodnota funkce
    return s;
}
void initSymbolTable()
{
	root = NULL;
}
tSymbolItemPtr insertIntoSymbolTable(tSymbolItemPtr *root, tSymbol symbol)
{
	int i;
	
	if (*root == NULL) {
		*root = malloc(sizeof(struct sSymbolItem));
		if (*root == NULL){
		    return NULL;
		}

		(*root)->symbol = symbol;
		(*root)->left = NULL;
		(*root)->right = NULL;
        
    } else {
        i = strcmp((*root)->symbol.key, symbol.key);

        if (i < 0) {
            return insertIntoSymbolTable( &((*root)->left), symbol);
            
        } else if (i > 0) {
            return insertIntoSymbolTable( &((*root)->right), symbol);
            
        } else {
            (*root)->symbol = symbol;
        }
    }

    return *root;
}
tSymbol *searchInSymbolTable(tSymbolItemPtr *root, char *key)
{
	if (*root == NULL || key == NULL) return NULL;
 	
	int i = strcmp((*root)->symbol.key, key);

	if (i < 0) {
		return searchInSymbolTable( &((*root)->left), key);
	
	} else if (i > 0) {
		return searchInSymbolTable( &((*root)->right), key);
	
	} else {
		return &(*root)->symbol;
	}
}
void printSubtree(tSymbolItemPtr *root)
{
	if(*root == NULL) return;
	
	const char* TYPES[] = {"VOID  ","BOOL  ","DOUBLE","INT   ","STRING"};
	tSymbol *s = &(*root)->symbol;
	printf("%-30s %s %s %s %s %3d# %d | ", s->key, TYPES[s->type], s->isFunction?"func":"var ", s->isDefined?"def":"und",s->nextArg==NULL?"no  ":"args", getIDbyPtr(s->begin), s->running);
	
	if(s->isDefined)
		switch(s->type){
			case T_BOOL:	printf("%12s",s->value.b?"true":"false");
				break;
			case T_DOUBLE:	printf("%12g",s->value.d);
				break;
			case T_INT:		printf("%12d",s->value.i);
				break;
			case T_STRING:	printf("\"%s\"",s->value.s);
				break;
			default:break;
		}
	printf("\n");
	
	printSubtree( &(*root)->left );
	printSubtree( &(*root)->right );
}
void printSymbolTable()
{
	printSubtree(&root);
	printf("\n");
}
void freeSubtree(tSymbolItemPtr *root)
{
	if(*root == NULL) return;
	freeSubtree( &(*root)->left );	// vycisti levy podstrom
	freeSubtree( &(*root)->right );	// vycisti pravy podstrom
	
	if((*root)->symbol.key != NULL)
		free((*root)->symbol.key);	// nazev symbolu
	if((*root)->symbol.isFunction){
		tSymbol *arg = (*root)->symbol.nextArg;
		while(arg != NULL){
			(*root)->symbol.nextArg = arg->nextArg;
			free(arg->key);
			free(arg);
			arg = (*root)->symbol.nextArg;
		}
		freeLocalScopeList( &((*root)->symbol.funcScope) );
	}
	free(*root);	// vycisti koren podstrom
	
	*root = NULL;
}
void freeSymbolTable()
{
	printf("Success - might throw segmentation fault because of trying to free static strings.\n");
	freeSubtree(&root);
}
/* SYMBOL TABLE END */



/* PARAM LIST */
int initParamList()
{
	gpl = malloc(sizeof(struct sParamList));
	if(gpl == NULL) return RC_InternalErr;
	
	gpl->first = NULL;
	gpl->last = NULL;
	
	return RC_OK;
}
tSymbol *insertParam(tSymbol *s)
{
	if (gpl == NULL
	||	s == NULL) return NULL;
	
	tParamItem *pi = malloc(sizeof(tParamItem));
	if(pi == NULL) return NULL;
	
	pi->s = s;
	
	if(gpl->first == NULL)
		gpl->first = pi;
	else	
		gpl->last->next = pi;
	pi->next = NULL;
	gpl->last = pi;
	
	return s;
}
tParamItem *getFirstParam()
{
	if(gpl == NULL) return NULL;
	return gpl->first;
}
void clearParamList()
{
	if(gpl == NULL) return;
	
	tParamItem *pi = gpl->first;
	while(pi != NULL){
		gpl->first = pi->next;
		free(pi);
		pi = gpl->first;
	}
}
void freeParamList()
{
	clearParamList();
	if(gpl != NULL)
		free(gpl);
}
/* PARAM LIST END */






char *shellSort(char *s)
{
	int n = strlen(s);
	int step = n / 2;
	int i;
	int j;
	char temp;

	if (s == NULL) return NULL;

	while (step > 0) {
		for (i = step; i < n; i++) {
			j = i - step;
			while (j >= 0 && s[j] > s[j+step]) {
				temp = s[j];
				s[j] = s[j+step];
				s[j+step] = temp;
				j = j - step;
			}
		}
		step = step / 2;
	}

	return s;
}

/* The Boyer-Moore string search algorithm
 *	char *search : hledany podretezec (vzorek)
 *	char *s : prohledavany retezec
 */
int BMFind(char *search, char *s)
{
	int lx = strlen(search);	//delka vzorku
	int lstr = strlen(s);		//delka prohledavaneho retezce 
	int ldiff = lstr - lx;		//rozdil delek obou retezcu
	int charJump[256];			//pole pro posledni vyskyty znaku
	int k = 0;					//zacatek prohledavani
	int i;

	// makeCharJumpArray(x, lx, charJump);

	// Init pole charJump
	for (i = 0; i < 256; i++) {
		charJump[i] = -1;
	}
	// kazdy znak z retezce x bude mit posledni vyskyt, znaky ktere nejsou v x maji hodnotu -1
	for (i = 0; i < lx; i++) {
		charJump[(int) search[i]] = i;
	}

	while (k <= ldiff) {
		int j = lx - 1; // nastavime index vzorku na konec

		while (j >= 0 && search[j] == s[k+j])	
			j--;	// kdyz se znaky shoduji, zkusime porovnat dalsi (resp. predchozi)

		if (j < 0) {
			// nasla se shoda, pozice je v indexu "k"
			// muzeme hned return pokud chceme pouze 1 shodu, nebo upravit funkci tak, aby nasla vice indexu a ulozila je do pole
			return k;
		}
		else {
			// nenaslo se, je potreba nastavit "k" se spravnym posunutim
			k += max(1, j - charJump[(int) s[k+j]]);
		}
	}
	return -1; // podretezec neni v retezci

}

int max (int a, int b)
{
	return (a > b) ? a : b;
}



