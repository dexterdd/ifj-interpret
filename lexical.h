/*
IFJ-Interpret jazyka IFJ16
lexical.h
Lexik�ln� anal�za

Varianta: b/3/I
T�m 093
   Mikul�k Petr.....xmikul57
   Matu� Adam.......xmatus31
   M�rka Filip......xmerka04
   Svoboda David....xsvobo0t
*/



#ifndef LEXICAL_H
#define LEXICAL_H
#include <string.h>
#include <stdio.h>
#include "error.h"
#include "ial.h"

//vycet rozparsovanych slov, takhle je budu posilat na syntaktickou anal�zu.
typedef enum{
	E_EOF,						//eof
	E_Identif,					//identifikator
	E_PKIdentif,					//plne kvalifikovany
	E_Assigment,					//=
	E_Semicolon,					//;
	E_Main,						//main
	E_Class,					//class
	E_Left_Curly_Bracket,				//{
	E_Right_Curly_Bracket,				//}
	E_Left_Bracket,					//(
    	E_Right_Bracket,				//)
	E_Static,					//static
	E_Void,						//void
	E_Comma,					//,
	E_Return,					//return
	E_If,						//if
	E_Else,						//else
	E_While,					//while
	E_Int,						//integer klicove slovo
	E_Int_Literal,					//integer cislo
	E_Double,					//double klicove slovo
	E_Double_Literal,				//double cislo
	E_String,					//String klicove slovo
	E_String_Literal,				//String cislo
	E_Plus,						//+ 
	E_Minus,					//-
	E_Multiple,					//*
	E_Divide,					// /
	E_Lesser,					//<
	E_Greater,					//>
	E_Lesser_Equal,					//<=
	E_Greater_Equal,				//>=
	E_Equal,					//==
	E_Not_Equal,					//!=
	E_Boolean,					//boolean
	E_Break,					//break
	E_Continue,					//continue
	E_Do,						//do
	E_False,					//false
	E_True,						//true
	E_For,						//for
} Token_type;


typedef struct SToken{
   Token_type typ;		  	//typ tokenu z vyctu (hodnota pro syntaktickou analizu)
   tValue value;			//hodnota, nazev identifikatoru
} tToken;

FILE *s_file;	 
tToken *token;
tToken *create_token();
void destroy_token(tToken *tok);
int get_token(tToken*);
int main();

#endif 

